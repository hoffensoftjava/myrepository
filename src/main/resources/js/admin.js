
AJS.toInit(function() 
{
    var baseUrl = AJS.params.baseURL;
    var tables;
    var fields;
    var schemeId = AJS.$("#schemeId").val();
    AJS.$("#btnAddRow").hide();
    if(schemeId)
      {
        loadSchemaFieldMapping();
      }
    loadProjectMappings();

    function fetchSNParentTables(isEdit,mappings)
    {
        var selected_tablename = AJS.$("#sn_Table_List").val();
        AJS.$.ajax({
            url: baseUrl + "/rest/services/1.0/servicenow/tables/table/parent/"+selected_tablename,
            dataType: "json",
            success: function(parentTables) 
              {
                  AJS.$("#parentTableList").attr("value",parentTables.name);
                  fetchSNTableSchema(isEdit,mappings);
              }
        });
    }

    function fetchSNTableSchema(isEdit,mappings)  
    {
        var selected_tablename = AJS.$("#sn_Table_List").val();
        var parent_tablename = AJS.$("#parentTableList").val();
        var tablenames= selected_tablename+","+ parent_tablename;
        AJS.$.ajax({
            url: baseUrl + "/rest/services/1.0/servicenow/tables/"+tablenames,
            dataType: "json",
            success: function(tablesArray) 
            {
                tables = tablesArray;
                $mappingTable =  AJS.$("#mapping_table");
                $mappingTable.html('');
                AJS.$.ajax({
                    url: baseUrl + "/rest/services/1.0/jira/issue",
                    dataType: "json",
                    success: function(fieldsArray) 
                    {
                        fields = fieldsArray;
                        if(isEdit == false)
                        {
                          buildTable(tablesArray,fieldsArray)  
                        }
                        else
                        {
                          buildEditTable(mappings,tablesArray,fieldsArray);  
                        }
                    }
                });
            }
        });
      }

      function loadSchemaFieldMapping()  
      {
          AJS.$('#spinner').show();
          var schemeId = AJS.$("#schemeId").val();

          AJS.$.ajax({
                url: baseUrl + "/rest/services/1.0/config/schemes/"+schemeId,
                dataType: "json",
                success: function(schemeObject) 
                {
                    $mappingTable =  AJS.$("#mapping_table");
                    $mappingTable.html('');
                    var parentTableList = AJS.$("#parentTableList").val();
                    if(parentTableList)
                    {
                      fetchSNTableSchema(true,schemeObject.mappings);  
                    }
                    else
                    {
                      fetchSNParentTables(true,schemeObject.mappings);
                    }
                    
                 }
          });
      }

    function buildEditTable(fieldMapping,tablesArray,fieldsArray)
    {
          var mapping_table = AJS.$("#mapping_table");
          var listItems = '<tbody> <tr><th class="th-style1">JIRA</th><th class="th-style2">SYNC TYPE</th><th class="th-style1">SERVICENOW</th></tr>';
          listItems += '<div class="box-body">';
          var index = 1;
          AJS.$.each(fieldMapping, function(index, option){
              listItems +='<tr class="tableRowClass">';
              listItems += '<td style="margin-bottom: 10px;">'
              listItems +='<select  class="form-control" name="JIRAField">';
              AJS.$.each(fields,function(index,fieldOption){
                      if(option.jiraFieldName == fieldOption.name)
                      {
                        listItems += '<option selected value="' + fieldOption.name + '">' + fieldOption.label + '</option>';    
                      }
                      else
                      {
                        listItems += '<option value="' + fieldOption.name + '">' + fieldOption.label + '</option>'; 
                      }
                     
               });
              listItems +='</select></td>';
              listItems +='<td>';
              listItems +='<select class="form-control" name="SyncType">';
              if(option.syncType == "PUSH")
                  {
                      listItems +='<option selected value="PUSH">PUSH</option>';
                      listItems +='<option value="PULL">PULL</option>';
                      listItems +='<option value="BOTH">BOTH</option>';
                  }
              else if(option.syncType == "PULL")
                  {
                      listItems +='<option value="PUSH">PUSH</option>';
                      listItems +='<option selected value="PULL">PULL</option>';
                      listItems +='<option value="BOTH">BOTH</option>';
                  }
              else
                  {
                      listItems +='<option value="PUSH">PUSH</option>';
                      listItems +='<option value="PULL">PULL</option>';
                      listItems +='<option selected value="BOTH">BOTH</option>';
                  }
              listItems +='</select></td>';
              listItems +='<td><select class="form-control" name="SNField">';
              AJS.$.each(tables, function(index, fieldOption){
                    if(fieldOption.label)
                    {
                      var valueOption = fieldOption.name + "," + fieldOption.type;
                      if(option.snFieldName == fieldOption.name)
                      {
                        listItems += '<option selected value="' + valueOption + '">' + fieldOption.label + '</option>';
                      }
                      else
                      {
                        listItems += '<option value="' + valueOption + '">' + fieldOption.label + '</option>';
                      }
                    }
              });
              listItems +='</select></td>';
              listItems +='<td>';
              listItems +='<input type="button" class="btn" value="Delete">';
              listItems +='</td>';
              listItems +='</tr>';
            });
          listItems += '</div></tbody>';
          mapping_table.append(listItems);
          AJS.$('#spinner').hide();
          AJS.$("#btnAddRow").show();
          deleteRow();
    }

  function buildTable(tables,fields)
  {
        var mapping_table = AJS.$("#mapping_table");
        var listItems = '<thead> <tr><th id="jira">JIRA</th><th id="sync">SYNC TYPE</th><th id="sn">SERVICENOW</th><th id="action">Action</th></tr></thead>';
        listItems += '<div class="box-body"><tbody>';
        var index = 1;
       // AJS.$.each(tables, function(index, option){
       listItems +='<tr class="tableRowClass">';
       listItems += '<td  headers="jira">'
       listItems +='<select required  name="JIRAField">';
       AJS.$.each(fields,function(index,option){
            listItems += '<option value="' + option.name + '">' + option.label + '</option>'; 
       });
       listItems +='</select></td>';
       listItems +='<td headers="sync">';
       listItems +='<select required name="SyncType">';
       listItems +='<option value="PUSH">PUSH</option>';
       listItems +='<option value="PULL">PULL</option>';
       listItems +='<option value="BOTH">BOTH</option>';
       listItems +='</select></td>';
       listItems +='<td headers="sn"><select required name="SNField">';
       AJS.$.each(tables, function(index, option){
            if(option.label)
                  {
                    var valueOption = option.name + "," + option.type;
                    listItems += '<option value="' + valueOption + '">' + option.label + '</option>';
                  }
        });
        listItems +='</select></td>';
        listItems +='<td headers="action">';
        listItems +='<input type="button" disabled class="btn" value="Delete"></td>';
        listItems +='</tr>';
          //});
        listItems += '/tbody></div><';
        mapping_table.append(listItems);
        AJS.$('#spinner').hide();
  }

  function addRow()
  {
      var listItems;
      var index = 1;
      listItems +='<tr headers="jira" class="tableRowClass">';
      listItems += '<td>'
      listItems +='<select required name="JIRAField">';
      AJS.$.each(fields,function(index,option){
            listItems += '<option value="' + option.name + '">' + option.label + '</option>'; 
      });
      listItems +='</select></td>';
      listItems +='<td headers="sync">';
      listItems +='<select required name="SyncType">';
      listItems +='<option value="PUSH">PUSH</option>';
      listItems +='<option value="PULL">PULL</option>';
      listItems +='<option value="BOTH">BOTH</option>';
      listItems +='</select></td>';
      listItems +='<td headers="sn"><select required name="SNField">';
      AJS.$.each(tables, function(index, option){
           if(option.label)
                {
                  var valueOption = option.name + "," + option.type;
                  listItems += '<option value="' + valueOption + '">' + option.label + '</option>';
                }
      });
      listItems +='</select></td>';
      listItems +='<td headers="action">';
      listItems +='<input type="button" class="btn" value="Delete">';
      listItems +='</td></tr>';
      AJS.$("#mapping_table tbody").append(listItems);
    }



    function deleteRow()
    {
        AJS.$(".btn").click(function(){
            AJS.$(this).closest('tr').remove();
        });
    }

    AJS.$("#sn_Table_List").change(function(e)
    {
        e.preventDefault();
        AJS.$('#spinner').show();
        AJS.$("#btnAddRow").show();
        fetchSNParentTables(false,null);
    });



    AJS.$("#btnAddRow").click(function(e)
    {
        e.preventDefault();
        addRow();
        deleteRow();
    });

  AJS.$("#save_config").submit(function(e)
  {
        e.preventDefault();
        var scheme = {};
        var endPointURL = "";
        var schemeId = AJS.$("#schemeId").val();
        var schemeName = AJS.$("#schemeName").val();
        var jiraObjectName = AJS.$("#jira_Table_List").val();
        var snObjectName = AJS.$("#sn_Table_List").val();
        var snParentObjects = AJS.$("#parentTableList").val();
        var mappings = [];
        AJS.$(".tableRowClass").each(function()
        {
          var jiraFieldName = AJS.$(this).find('select[name=JIRAField]').val();
          var arr = AJS.$(this).find('select[name=SNField]').val().split(',');
          var syncType = AJS.$(this).find('select[name=SyncType]').val();
          var snFieldName = arr[0];
          var snFieldType = arr[1];
          mappings.push({
              jiraFieldName : jiraFieldName,
              snFieldName : snFieldName,
              syncType : syncType,
              snFieldType: snFieldType
          });
        });
    scheme["schemeName"] = schemeName;
    scheme["jiraObjectName"] = jiraObjectName;
    scheme["snObjectName"] = snObjectName;
    scheme["snParentObjects"] = snParentObjects;
    scheme["mappings"] = mappings;
    if(mappings.length <= 0)
    {
      initErrorDlg("Please select atleast one mapping fields").show();
      return;
    }
    if(schemeId)
    {
      scheme["id"] = schemeId;
      endPointURL = baseUrl + "/rest/services/1.0/config/scheme/"+ schemeId
    }
    else
    {
      endPointURL = baseUrl + "/rest/services/1.0/config/schemes"
    }
    var jsonString = JSON.stringify(scheme);
    AJS.$.ajax({
          url: endPointURL,
          type: "POST",
          dataType: "json",
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
          },
          data: jsonString,
          success: function(tablesArray) 
          {
              if(schemeId)
              {
                alert("Scheme Updated Successfully!");
              }
              else
              {
                alert("Scheme Saved Successfully!");
              }
              location.href = baseUrl + "/secure/SchemeList!SchemeListDefault.jspa";
          },
           processData:false
    });
  });



  function initErrorDlg(bodyText) {
    var errorDialog = new AJS.Dialog({
        width:420,
        height:250,
        id:"error-dialog",
        closeOnOutsideClick: false
    });

    errorDialog.addHeader("Error");
    errorDialog.addPanel("ErrorMainPanel", '' +
        '<html><body><div class="error-message errdlg">' +
        bodyText +
        '</div></body></html>',
        "error-panel-body");
    errorDialog.addCancel("Cancel", function() {
        errorDialog.hide();
    });

    return errorDialog;
}


  AJS.$("#save_Project_Mapping").submit(function(e)
  {
    e.preventDefault();
    
    var scheme = {};
    var projectKeys = AJS.$("#project_List").val();
    var SchemeName = AJS.$("#scheme_List").val();
    var Intervel = 0;//AJS.$("#intervel_time").val();
    var Duration = "MINUTES";//AJS.$("#intervel_duration").val();
    var projectMappings = [];
    //AJS.$.each(projectKeys, function( index, value ) {
      var projectMapping = {};
      scheme["id"] = SchemeName;
      projectMapping["projectName"] = projectKeys,
      projectMapping["scheme"] = scheme,
      projectMapping["interval"] = Intervel,
      projectMapping["duration"] = Duration;
      projectMappings.push(projectMapping);
    //});
    var jsonString = JSON.stringify(projectMappings);
    AJS.$.ajax({
      url: baseUrl + "/rest/services/1.0/config/projectmappings",
      type: "POST",
      dataType: "json",
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json' 
      },
      data: jsonString,
      success: function(result) {
        alert("Project Scheme Mapping Saved Successfully!");
        //location.reload(true);
        loadProjectMappings();
       },
       processData:false
      });
  });

  function loadProjectMappings()
  {
        AJS.$.ajax({
        url: baseUrl + "/rest/services/1.0/config/projectmappings",
        dataType: "json",
        success: function(projectMappingArray) {
          buildProjectMappingTable(projectMappingArray);
          var str = location.href.replace("#","");
          AJS.$("#projectDiv").load(str+" #projectDiv",function(){});
         }
        });
  }

  function deleteProjectMappings(pmid)
  {
    var answer = confirm("Are you sure you want to remove this?")
      if (answer)
      {
        AJS.$.ajax({
        url: baseUrl + "/rest/services/1.0/config/projectmapping/"+pmid,
        type: "DELETE",
        dataType: "json",
        success: function(result) {
          alert("Project Mapping Deleted Successfully");
          loadProjectMappings();
          var str = location.href.replace("#","");
          AJS.$("#projectDiv").load(str+" #projectDiv",function(){});
         }
        });
      }    
  }

  
  function buildProjectMappingTable(projectMappings)
    {
    var mapping_table = AJS.$("#project_list_table");
    mapping_table.html('');
    var listItems = '<tbody> <tr><th class="th-style1">action</th><th class="th-style1">Scheme</th><th class="th-style1">projectName</th><th class="th-style1">Duration</th></tr>';
    listItems += '<div class="box-body">';
    var index = 1;
    AJS.$.each(projectMappings, function(index, option){
        listItems +='<tr class="tableRowClass">';
          listItems += '<td style="margin-bottom: 10px;">';
          listItems += '<a class="delA" name='+ option.id +' href="#">Delete</a>';
          listItems += '</td>';
          listItems += '<td>';
          listItems += '<a class="form-control" name="schemeName" href="#">'+ option.scheme.schemeName +'</a>';
          listItems +='</td>';
          listItems +='<td>';
          listItems += '<a href="#">'+ option.projectName +'</a>';
          listItems +='</td>';
          listItems +='<td>';
          listItems += '<a href="#">'+ option.interval +'-'+ option.duration +'</a>';
          listItems +='</td>';
        listItems +='</tr>';
      });
    listItems += '</div></tbody>';
    mapping_table.append(listItems);
    deleteMappingRow();
    }

  function deleteMappingRow()
  {
    AJS.$(".delA").click(function(){
      deleteProjectMappings(AJS.$(this).attr('name'));
    });
  }

    function rMoveAll() {
        AJS.$('#project_List option').remove().appendTo('#selectedProject'); 
    }
    
     function lMoveAll() {
        AJS.$('#selectedProject option').remove().appendTo('#project_List'); 
    }

    function rMoveSelected() {
        AJS.$('#project_List option:selected').remove().appendTo('#selectedProject'); 
    }
    function lMoveSelected() {
        AJS.$('#selectedProject option:selected').remove().appendTo('#project_List'); 
    }
    function selectAll() {
        AJS.$("select option").attr("selected","selected");
    }

    AJS.$("#RmoveAll").click(rMoveAll);
    AJS.$("#RmoveSelected").click(rMoveSelected);
    AJS.$("#LmoveAll").click(lMoveAll);
    AJS.$("#LmoveSelected").click(lMoveSelected);
});

function deleteScheme(scid)
  {
    var baseUrl = AJS.params.baseURL;
    var answer = confirm("Deleting the scheme will delete all the associated FieldMapping as well as Project Mapping.Are you sure?");
      if (answer)
      {
        AJS.$.ajax({
        url: baseUrl + "/rest/services/1.0/config/scheme/"+scid,
        type: "DELETE",
        dataType: "json",
        success: function(result) {
          alert("Scheme Deleted Successfully");
          var str = location.href.replace("#","");
          AJS.$("#schemeDiv").load(str+" #schemeDiv",function(){});
         }
        });
      }    
  }


