package com.example.tutorial.plugins.admin.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;


import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import net.java.ao.Query;
import java.util.Map;
import java.util.Enumeration;
import java.util.ArrayList; 
import java.util.List;
import java.util.*;
import java.util.Set;
import java.util.Comparator;

import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import org.springframework.util.StringUtils;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.SyncTransaction;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.XmlElement.XMLSNTable;
import com.example.tutorial.plugins.admin.util.FieldCollectionsUtils;
import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;

import java.text.SimpleDateFormat;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.MutableIssue;
import com.example.tutorial.plugins.admin.util.*;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.event.type.EventDispatchOption;



@Path("/servicenow/tables")
@Named("SNServicesResource")
public class SNServicesResource
{
	@ComponentImport
    private final IssueManager issueManager;
    @ComponentImport
    private final CommentManager commentManager;
  	@ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	@ComponentImport
    private final TransactionTemplate transactionTemplate;
    private ServiceNowClient serviceNowClient;
    private AOMappingService aoMappingService;
    private static ServiceNowFieldUtils serviceNowFieldUtils;
  	private static FieldCollectionsUtils fieldCollectionsUtils;
  	private static IssueActionsUtils issueActionsUtils;

    private static final Logger log;
    static {
        log = LoggerFactory.getLogger(AdminServices.class);
    }

   	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    private final String SYNC_FIELD_NAME = "SNSYNC";
    private final String SN_COMMENT_SUFFIX = "COMMENT : %s. COMMENT FROM : SERVICENOW BY : %s AT %s";

 	@Inject
  	public SNServicesResource(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,AOMappingService aoMappingService,TransactionTemplate transactionTemplate,
        ServiceNowClient serviceNowClient,IssueManager issueManager,CommentManager commentManager,
        ServiceNowFieldUtils serviceNowFieldUtils,FieldCollectionsUtils fieldCollectionsUtils,IssueActionsUtils issueActionsUtils)
  	{
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.serviceNowClient = serviceNowClient;
        this.aoMappingService = aoMappingService;
        this.transactionTemplate = transactionTemplate;
        this.issueManager = issueManager;
        this.commentManager = commentManager;
        this.serviceNowFieldUtils = serviceNowFieldUtils;
      	this.issueActionsUtils = issueActionsUtils;
      	this.fieldCollectionsUtils = fieldCollectionsUtils;
  	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTables()
	{

		String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}

		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        	{
	            JSONArray jsonResult = new JSONObject(serviceNowClient.fetchAllTables()).getJSONArray("result");
	            List<XMLSNTable> tableList = new ArrayList<XMLSNTable>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	XMLSNTable table = new XMLSNTable();
	                JSONObject tableJSON = jsonResult.getJSONObject(i);
	                if(!tableJSON.getString("name").isEmpty())
	                {
		                table.setName(tableJSON.getString("name"));
		                table.setLabel(tableJSON.getString("label"));
		                table.setSysId(tableJSON.getString("sys_id"));
		                if(tableJSON.has("sys_package") && !tableJSON.isNull("sys_package") && !tableJSON.getString("sys_package").isEmpty())
		                {
		                    JSONObject jsonSysPackage = tableJSON.getJSONObject("sys_package");
		                    table.setLink(jsonSysPackage.getString("link"));
		                }
		                tableList.add(table);
	            	}
            }
            	if(tableList.size() > 0)
            	{
                	Collections.sort(tableList,XMLSNTable.TableLabelComparator);
            	}
           		return tableList;
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
	}

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("table/{key}")
    public Response getTable(@PathParam("key") final String key)
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        	{
        		/*
        			SN Object Name : sys_dictionary
        			Column Name is "element"
        			Column Label is "column_label"
				*/	
	            JSONArray jsonResult = new JSONObject(serviceNowClient.fetchAllTableColumnNames(key)).getJSONArray("result");
	            List<XMLSNTable> tableList = new ArrayList<XMLSNTable>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	XMLSNTable table = new XMLSNTable();
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.getString("internal_type") != "Collection" && !tableJSON.getString("column_label").isEmpty())
	            	{
		                table.setName(tableJSON.getString("element"));
		                table.setLabel(tableJSON.getString("column_label"));
		                table.setSysId(tableJSON.getString("sys_id"));
		                JSONObject jsonSysPackage = tableJSON.getJSONObject("internal_type");
		                table.setType(jsonSysPackage.getString("value"));
		                tableList.add(table);
	            	}
            	}
            	if(tableList.size() > 0)
            	{
                	Collections.sort(tableList,XMLSNTable.TableLabelComparator);
            	}
           		return tableList;
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("table/parent/{key}")
    public Response getAllParentTables(@PathParam("key") final String key)
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        	{
	            String parentTableList = serviceNowClient.fetchAllParentTables(key);
				XMLSNTable xmltable = new XMLSNTable();
                xmltable.setName(parentTableList);
           		return xmltable;
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{key}")
    public Response getTableColumn(@PathParam("key") final String key)
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        	{
	            JSONArray jsonResult = new JSONObject(serviceNowClient.fetchAllTableColumnNames(key)).getJSONArray("result");
	            List<XMLSNTable> tableList = new ArrayList<XMLSNTable>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {	

	            	XMLSNTable table = new XMLSNTable();
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	/*
	            	JSONArray columnArray = tableJSON.names();
	            	for(int j = 0; j < columnArray.length(); j++)
	            	{
	            		XMLSNTable table = new XMLSNTable();
	            		table.setName(columnArray.getString(j));
		                table.setLabel(columnArray.getString(j));
		                tableList.add(table);
	            	}
	            	*/
	            	if(tableJSON.getString("internal_type") != "Collection" && !tableJSON.getString("column_label").isEmpty())
	            	{
		                table.setName(tableJSON.getString("element"));
		                table.setLabel(tableJSON.getString("column_label"));
		                table.setSysId(tableJSON.getString("sys_id"));
		                 JSONObject jsonSysPackage = tableJSON.getJSONObject("internal_type");
		                table.setType(jsonSysPackage.getString("value"));
		                tableList.add(table);
	            	}
	            	
            	}
            	if(tableList.size() > 0)
            	{
                	Collections.sort(tableList,XMLSNTable.TableLabelComparator);
            	}
           		return tableList;
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("checking")
    @Deprecated
    public Response updateTicketsChecking()
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        	{
	        	Iterable<Scheme> schemes = aoMappingService.allSchemes();
	        	Set<Long> issueIds = new HashSet<Long>();
	        	Map<String,String> specialFieldMap = new HashMap<String,String>();
        		Map<String,String> updateNormalFieldResponseMap = new HashMap<String,String>();
        		Map<String,String> updateSpecialFieldResponseMap = new HashMap<String,String>();
        		Date currentDate = new Date();
        		StringBuilder sysparam_Field = new StringBuilder();
        		//currentDate.setTime(currentDate.getTime() - 2 * 24 * 3600 * 1000);
        		currentDate.setTime(currentDate.getTime() - 1 * 1 * 600 * 1000);
        		for(Scheme scheme : schemes) {
	        		String snTableName = scheme.getSNObjectName();

	        		for(FieldMapping field : scheme.getMappings())
		        	{
		        		if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()) != null)
		        		{
		        			if(specialFieldMap.get(field.getSNFieldName()) == null)
		        			{
		        				specialFieldMap.put(field.getSNFieldName(),serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()));
		        			}
		        		}
		        		else
		        		{
		        			sysparam_Field.append(field.getSNFieldName());
		        			sysparam_Field.append(",");
		        		}
		        	}

	        		String snUpdateResponse = serviceNowClient.retrieveUpdatesFromServicenNow(snTableName,currentDate,sysparam_Field.toString());
	        		fetchSysIds(snUpdateResponse,updateNormalFieldResponseMap);
	        		for(ProjectMapping proMap : scheme.getProjectMappings())
	        		{
	        			Set<Long> ids = aoMappingService.fetchSyncTransaction(updateNormalFieldResponseMap.keySet(),proMap.getProjectName()); 	
	        			issueIds.addAll(ids);
	        		}

	        	}
	        	if(!specialFieldMap.isEmpty())
        		{
	        		for(String sysId:updateNormalFieldResponseMap.keySet())
					{
						for(String specialField : specialFieldMap.keySet())
						{
							getSpecialFieldValues(sysId,specialField,specialFieldMap.get(specialField),updateSpecialFieldResponseMap,currentDate);	
						}
					}
        		}
        		List<Issue> issueCollections = issueManager.getIssueObjects(issueIds);
        		for(Issue issue : issueCollections)
				{
					String snID = issueActionsUtils.getFieldStringValue(issue,fieldCollectionsUtils.getFieldIdByName("SNID"));
					String updateNormalFieldResponse = updateNormalFieldResponseMap.get(snID);
					String updateSpecialFieldResponse = updateSpecialFieldResponseMap.get(snID);
					updateSpecialValueIntoIssue(issue,updateNormalFieldResponse,updateSpecialFieldResponse);
					issueManager.updateIssue(currentAppUser,(MutableIssue)issue,EventDispatchOption.DO_NOT_DISPATCH,false);
				}
	        	return StringUtils.collectionToCommaDelimitedString(issueIds);
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }

        private void getSpecialFieldValues(String sysId,String fieldName,String endPointAPI,Map<String,String> updateSpecialFieldResponseMap,Date currentDate)
        {
        	try
        	{
	        	String specialFieldResponse = serviceNowClient.fetchSpecialFieldValues(sysId,fieldName,endPointAPI,currentDate);
	        	if(updateSpecialFieldResponseMap.get(sysId) == null)
	        	{
	        		updateSpecialFieldResponseMap.put(sysId,specialFieldResponse);
	        	}
        	}
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}

        }

        private void updateSpecialValueIntoIssue(Issue issue,String updateNormalFieldResponse,String updateSpecialFieldResponse)
        {
        	try
        	{
	        	Scheme scheme = aoMappingService.getSchemeForProject(issue.getProjectObject().getKey());
	        	// log.info("scheme {}",scheme);
	        	IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
	        	for(FieldMapping fieldMapping : scheme.getMappings())
	        	{
	        		String snFieldValue = "";
	        		String jiraFieldKey = fieldMapping.getJIRAFieldName();
					String snFieldKey = fieldMapping.getSNFieldName();
					if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(fieldMapping.getSNFieldType().toUpperCase())!= null)
					{
						if(updateSpecialFieldResponse!=null)
						{
							JSONArray jsonResult = new JSONObject(updateSpecialFieldResponse).getJSONArray("result");
							for (int i = 0; i < jsonResult.length(); i++)
							{
								JSONObject tableJSON = jsonResult.getJSONObject(i);
								if(tableJSON.has("value") && !tableJSON.getString("value").isEmpty())
								{
									snFieldValue = tableJSON.getString("value");
									String createdBy = tableJSON.getString("sys_created_by");
									String createdOn = tableJSON.getString("sys_created_on");
									snFieldValue = String.format(SN_COMMENT_SUFFIX,snFieldValue,createdBy,createdOn);
									issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(jiraFieldKey),snFieldValue,changeHolder);	
								}
							}
						}
					}
					else
					{
						if(updateNormalFieldResponse !=null)
						{
						 	JSONObject jsonString = new JSONObject(updateNormalFieldResponse);
						 	snFieldValue= jsonString.getString(snFieldKey);	
						 	issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(jiraFieldKey),snFieldValue,changeHolder);							
						}
					}
	        	}
	        	log.debug("Updating Identifier Field using setFieldValue {}",scheme);
	        	issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(SYNC_FIELD_NAME),"true",changeHolder);							
        	}
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

        private Object getValueFromJson(JSONObject jObject,String searchKey,Object obj) throws JSONException 
        {
        	Iterator<?> keys = jObject.keys();
        	log.info("jObject.get(key){}",keys);
				while( keys.hasNext()) {
					String key = (String)keys.next();
					if (key.equals(searchKey)) {
						
						obj = jObject.get(key);
						log.info("jObject.get(key)2{}",obj);
					}
					else if (jObject.get(key) instanceof JSONObject ) {
						obj = getValueFromJson((JSONObject)jObject.get(key), searchKey,obj);
						log.info("jObject.get(key)4{}",obj);
					}
					else if (jObject.get(key) instanceof JSONArray ) {
						JSONArray jar = (JSONArray)jObject.get(key);
						for (int i = 0; i < jar.length(); i++) {
							JSONObject j = jar.getJSONObject(i);
							log.info("jObject.get(key)1{}",j);
							getValueFromJson(j, searchKey,obj);
						}
					}
    			}
    			log.info("jObject.get(key)3{}",obj);
    			return obj;
        }

        private void fetchSysIds(String snUpdateResponse,Map<String,String> updateNormalFieldResponseMap) 
        {
        	try
        	{
        	JSONArray jsonResult = new JSONObject(snUpdateResponse).getJSONArray("result");
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		if(updateNormalFieldResponseMap.get(tableJSON.getString("sys_id")) == null)
	            		{
	            			updateNormalFieldResponseMap.put(tableJSON.getString("sys_id"),tableJSON.toString());
	            		}
	            	}
            	}
            }
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

    private Set<String> fetchSysIds(String snUpdateResponse) 
        {
        	try
        	{
        	JSONArray jsonResult = new JSONObject(snUpdateResponse).getJSONArray("result");
	            Set<String> tableSets = new HashSet<String>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		tableSets.add(tableJSON.getString("sys_id"));
	            	}
            	}
            	return tableSets;
            	      }
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }
 

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("checking/{key}")
    @Deprecated
    public Response dateChecking(@PathParam("key") final String key)
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    	String resultDateString = "";
		    try
        	{				
				Date currentDate = new Date();
				currentDate.setTime(new Date().getTime() - 3600 * 6000);
				String response = ""; //serviceNowClient.retrieveUpdatesFromServicenNow(key,currentDate);

				JSONArray jsonResult = new JSONObject(response).getJSONArray("result");
	            Set<String> tableSets = new HashSet<String>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		tableSets.add(tableJSON.getString("sys_id"));
	            	}
            	}

            	String str = StringUtils.collectionToCommaDelimitedString(tableSets);
				return str;
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }


	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("ao")
    public Response dateChecking()
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    	return aoMappingService.getAllSyncTransaction();
        	}
        	
	  })).build();
    }


/*
    1.Pass only the required fields into Servicenow API
	2.get the last updated from the table which has special field
	3.compare the both result and collect only the required sys records and store it in Collection.
	4.Loop the collection and fetch the requried fields and update into IssueManager.
*/
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("checking1")
    @Deprecated
    public Response updateTicketsChecking1()
    {
        String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}
		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    	String resultDateString = "";
		    try
        	{
	        	Iterable<Scheme> schemes = aoMappingService.allSchemes();
	        	Set<Long> issueIds = new HashSet<Long>();
        		for(Scheme scheme : schemes) {
	        		String snTableName = scheme.getSNObjectName();
	        		Date currentDate = new Date();
	        		currentDate.setTime(currentDate.getTime() - 1 * 24 * 3600 * 1000);
	        		log.info("@#$@#$@#$ Selected Date: {}",currentDate);
	        		String snUpdateResponse = serviceNowClient.retrieveUpdatesFromServicenNow(snTableName,currentDate,"");
	        		Set<String> sysIds = fetchSysIds(snUpdateResponse);
	        		log.info("@#$@#$@#$ Updated Sys_Ids: {}",StringUtils.collectionToCommaDelimitedString(sysIds));
	        		
	        		for(ProjectMapping proMap : scheme.getProjectMappings())
	        		{
	        			Set<Long> ids = aoMappingService.fetchSyncTransaction(sysIds,proMap.getProjectName());
	        			log.info("@#$@#$@#$ Related Issue Ids: {}",StringUtils.collectionToCommaDelimitedString(ids)); 	
	        			issueIds.addAll(ids);
	        		}
	        	}
	        	log.info("@#$@#$@#$ Final List of Issues: {}",StringUtils.collectionToCommaDelimitedString(issueIds));
	        	List<Issue> issueCollections = issueManager.getIssueObjects(issueIds);
	        	for(Issue issue : issueCollections)
	        	{
	        		MutableIssue mutableissue = issueManager.getIssueObject(issue.getId());
	           		String body = "New Comment Updated for testing";
	        		commentManager.create(mutableissue,currentAppUser,body,true);
	        	}


	        	return StringUtils.collectionToCommaDelimitedString(issueIds);
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
	   	}
	  })).build();
    }


}


