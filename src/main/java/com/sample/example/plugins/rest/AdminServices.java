package com.example.tutorial.plugins.admin.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;


import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import net.java.ao.Query;
import java.util.Map;
import java.util.Enumeration;
import java.util.ArrayList; 
import java.util.List;
import java.util.*;
import java.util.Set;
import java.util.Comparator;

import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;


import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.XmlElement.XMLScheme;
import com.example.tutorial.plugins.admin.XmlElement.XMLFieldMapping;
import com.example.tutorial.plugins.admin.XmlElement.XMLProjectMapping;
import com.atlassian.jira.issue.fields.CustomField;
import com.example.tutorial.plugins.admin.util.FieldCollectionsUtils;
import com.example.tutorial.plugins.admin.util.IssueActionsUtils;


@Path("/config")
@Named("AdminServices")
public class AdminServices
{

  	@ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	  @ComponentImport
    private final TransactionTemplate transactionTemplate;
    private AOMappingService aoMappingService;
    
    private FieldCollectionsUtils fieldCollectionsUtils;
    private IssueActionsUtils issueActionsUtils;


    private static final Logger log;
    static {
        log = LoggerFactory.getLogger(AdminServices.class);
    }
    //

 	  @Inject
  	public AdminServices(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,TransactionTemplate transactionTemplate,AOMappingService aoMappingService,
        FieldCollectionsUtils fieldCollectionsUtils,IssueActionsUtils issueActionsUtils)
        
  	{
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.transactionTemplate = transactionTemplate;
        this.aoMappingService = aoMappingService;
        this.fieldCollectionsUtils = fieldCollectionsUtils;
        this.issueActionsUtils = issueActionsUtils;
  	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/schemes")
	public Response addScheme(final XMLScheme xmlScheme)
	{

		String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}

		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        {
          //Scheme scheme = aoMappingService.getScheme(xmlProjectMapping[0].scheme.id);
          //aoMappingService.deleteProjectMappings(xmlProjectMapping[0].scheme.id);  
           Scheme scheme = aoMappingService.addScheme(xmlScheme.schemeName,xmlScheme.jiraObjectName,xmlScheme.snObjectName);
           
           for (XMLFieldMapping fieldMapping : xmlScheme.mappings)
           {
              aoMappingService.addFieldMapping(scheme,fieldMapping.jiraFieldName,fieldMapping.snFieldName,FieldMapping.SyncType.valueOf(fieldMapping.syncType.toUpperCase()),fieldMapping.snFieldType);
           }
            return loadSchemeIntoXML(scheme);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
	   	}
	  })).build();
	}
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/scheme/{id}")
  public Response updateScheme(@PathParam("id") final int xmlSchemeID,final XMLScheme xmlScheme)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }

    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
        {
          log.info("######SchemeID1 {}",xmlSchemeID);
           aoMappingService.deleteFieldMappings(xmlScheme.id);  
           Scheme scheme = aoMappingService.updateScheme(xmlScheme.id,xmlScheme.schemeName,xmlScheme.jiraObjectName,xmlScheme.snObjectName);
           
           for (XMLFieldMapping fieldMapping : xmlScheme.mappings)
           {
              aoMappingService.addFieldMapping(scheme,fieldMapping.jiraFieldName,fieldMapping.snFieldName,FieldMapping.SyncType.valueOf(fieldMapping.syncType.toUpperCase()),fieldMapping.snFieldType);
           }
            return loadSchemeIntoXML(scheme);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
      }
    })).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/schemes")
  public Response getSchemes()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              Iterable<Scheme> schemeVariables = aoMappingService.allSchemes();
             return loadSchemeListIntoXML(schemeVariables);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }


  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/schemes/{id}")
  public Response getScheme(@PathParam("id") final int xmlSchemeID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              Scheme scheme = aoMappingService.getScheme(xmlSchemeID);
              return loadSchemeIntoXML(scheme);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }

@DELETE
  @Path("/scheme/{id}")
  @Produces("text/plain")
  public Response deleteScheme( @PathParam("id") final int schemeID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    transactionTemplate.execute(new TransactionCallback()
    {
        @Override
        public Object doInTransaction()
        {
        try
        {
          Scheme schme = aoMappingService.getScheme(schemeID);
          aoMappingService.deleteScheme(schme);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return null;
      }
    });
    return Response.noContent().build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/fieldmapping/{id}")
  public Response getFieldMapping(@PathParam("id") final int xmlFieldMappingID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              FieldMapping fieldMapping = aoMappingService.getFieldMapping(xmlFieldMappingID);
              XMLFieldMapping xmlMapping = new XMLFieldMapping();
              xmlMapping.id= fieldMapping.getID();
              xmlMapping.jiraFieldName= fieldMapping.getJIRAFieldName();
              xmlMapping.snFieldName = fieldMapping.getSNFieldName(); 
              xmlMapping.syncType = fieldMapping.getSyncType().toString();

              XMLScheme tempScheme = new XMLScheme();
              tempScheme.id = fieldMapping.getScheme().getID();
              tempScheme.schemeName = fieldMapping.getScheme().getSchemeName();
              tempScheme.jiraObjectName = fieldMapping.getScheme().getJIRAObjectName();
              tempScheme.snObjectName = fieldMapping.getScheme().getSNObjectName();
              xmlMapping.scheme = tempScheme;

              return xmlMapping;
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/projectmappings")
  public Response getProjectMappings()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          { 
              //aoMappingService.deleteALLProjectMappings();
              Iterable<ProjectMapping> projectMappingVariables = aoMappingService.allProjectMappings();
              return loadProjectMappingListIntoXML(projectMappingVariables);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }


  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/projectmappings/{project}")
  public Response getProjectMapping(@PathParam("project") final String xmlProjectName)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              
              ProjectMapping projectMapping = aoMappingService.getProjectMapping(xmlProjectName);
              return loadProjectMappingIntoXML(projectMapping);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/projectmappings/scheme/{id}")
  public Response getProjectMapping(@PathParam("id") final int schemeID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {

              Scheme scheme = aoMappingService.getScheme(schemeID);
              return loadSchemeIntoXML(scheme);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }


  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/projectmappings")
  public Response addProjectMappings(final XMLProjectMapping[] xmlProjectMapping)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }

    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
        {
          Scheme scheme = aoMappingService.getScheme(xmlProjectMapping[0].scheme.id);
          //aoMappingService.deleteProjectMappings(xmlProjectMapping[0].scheme.id);  
          XMLProjectMapping xmlProMapping = xmlProjectMapping[0];
            boolean isAlreadyExists = false;
            for(ProjectMapping proMap : scheme.getProjectMappings())
            {
                if(proMap.getProjectName().equalsIgnoreCase(xmlProMapping.projectName)) //Assume there is only one Project Mapping 
                {
                  isAlreadyExists = true;
                }
            }
            log.info("$$$$$$$$$xmlProMapping.projectName {}",isAlreadyExists);
            if(!isAlreadyExists)
            {
              ProjectMapping projectMapping = aoMappingService.addProjectMapping(scheme,xmlProMapping.projectName,xmlProMapping.interval,ProjectMapping.Duration.valueOf(xmlProMapping.duration.toUpperCase()));
            }
            scheme = aoMappingService.getScheme(xmlProjectMapping[0].scheme.id);
            return loadSchemeIntoXML(scheme);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
      }
    })).build();
  }

 


  @DELETE
  @Path("/projectmapping/{id}")
  @Produces("text/plain")
  public Response deleteProjectMapping( @PathParam("id") final int projectMappingID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    transactionTemplate.execute(new TransactionCallback()
    {
        @Override
        public Object doInTransaction()
        {
        try
        {
          aoMappingService.deleteProjectMapping(projectMappingID);
          //scheme = aoMappingService.getScheme(xmlProjectMapping[0].scheme.id);
          //return "OK";//loadSchemeIntoXML(scheme);
          
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return null;
      }
    });
    return Response.noContent().build();
  }


  private List<XMLProjectMapping> loadProjectMappingListIntoXML(Iterable<ProjectMapping> projectMappingVariables)
  {
        List<XMLProjectMapping> xmlProjectMappingList = new ArrayList<XMLProjectMapping>(); 
        for(ProjectMapping projectMapping : projectMappingVariables) {
          xmlProjectMappingList.add(loadProjectMappingIntoXML(projectMapping));
        }
        return xmlProjectMappingList;
  }


  private XMLProjectMapping loadProjectMappingIntoXML(ProjectMapping projectMappingVariable)
  {
        
        XMLProjectMapping xmlProjectMapping = new XMLProjectMapping();
        xmlProjectMapping.id = projectMappingVariable.getID();

        XMLScheme tempScheme = new XMLScheme();
        tempScheme.id = projectMappingVariable.getScheme().getID();
        tempScheme.schemeName = projectMappingVariable.getScheme().getSchemeName();
        tempScheme.jiraObjectName = projectMappingVariable.getScheme().getJIRAObjectName();
        tempScheme.snObjectName = projectMappingVariable.getScheme().getSNObjectName();
        xmlProjectMapping.scheme = tempScheme;

        xmlProjectMapping.projectName = projectMappingVariable.getProjectName();
        xmlProjectMapping.interval = projectMappingVariable.getInterval();
        xmlProjectMapping.duration = projectMappingVariable.getDuration().toString().toLowerCase();
        return xmlProjectMapping;
  }


  private List<XMLScheme> loadSchemeListIntoXML(Iterable<Scheme> schemeVariables)
  {
        List<XMLScheme> xmlSchemeList = new ArrayList<XMLScheme>(); 
        for(Scheme scheme : schemeVariables) {
          xmlSchemeList.add(loadSchemeIntoXML(scheme));
        }
        return xmlSchemeList;
  }

  private XMLScheme loadSchemeIntoXML(Scheme scheme)
  {
       XMLScheme xmlScheme = new XMLScheme();
       xmlScheme.id = scheme.getID();
       xmlScheme.schemeName = scheme.getSchemeName();
       xmlScheme.jiraObjectName = scheme.getJIRAObjectName();
       xmlScheme.snObjectName = scheme.getSNObjectName();
       FieldMapping[] mappings = scheme.getMappings();
       List<XMLFieldMapping> mappingList = new ArrayList<XMLFieldMapping>(); 
       for (int i = 0; i < mappings.length; i++) {
          XMLFieldMapping mapping = new XMLFieldMapping();
          mapping.id= mappings[i].getID();
          mapping.jiraFieldName= mappings[i].getJIRAFieldName();
          mapping.snFieldName = mappings[i].getSNFieldName(); 
          mapping.syncType = mappings[i].getSyncType().toString();
          mappingList.add(mapping);
      }
      xmlScheme.mappings = mappingList;
      List<XMLProjectMapping> proMappingList = new ArrayList<XMLProjectMapping>();
      ProjectMapping[] proMappings = scheme.getProjectMappings();
      for (int i = 0; i < proMappings.length; i++) {
          XMLProjectMapping projectMapping = new XMLProjectMapping();
          projectMapping.id= proMappings[i].getID();
          projectMapping.projectName= proMappings[i].getProjectName();
          projectMapping.duration = proMappings[i].getDuration().toString(); 
          projectMapping.interval = proMappings[i].getInterval();
          proMappingList.add(projectMapping);
      }
      xmlScheme.projectMappings = proMappingList;
       return xmlScheme;
  }


  @GET
  @Produces("text/plain")
  @Path("/outbound/result/{key}")
  public Response getAllAoFields(@PathParam("key") final String issueKey)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {

          Issue issueObject = ComponentAccessor.getIssueManager().getIssueByCurrentKey(issueKey);
          Scheme scheme = aoMappingService.getSchemeForProject(issueObject.getProjectObject().getKey());
          StringBuilder output = new StringBuilder();
          output.append("{");
          try
          {
          if(scheme == null)
          {
              output.append("No Scheme assigned for this Project.");
          }
          else
          {
                
                List<Field> issueFields =fieldCollectionsUtils.getAvailableFields();
                
                for(FieldMapping fieldMap : scheme.getMappings())
                {
                  if(fieldMap.getSyncType() != FieldMapping.SyncType.PULL)
                  {
                    for(Field field:issueFields)
                    {
                      if(field.getName().equals(fieldMap.getJIRAFieldName()))
                      {
                        output.append(String.format("\"%s\":\"%s\",",fieldMap.getSNFieldName(),issueActionsUtils.getFieldValueFromIssue(issueObject,field)));
                      }
                    }
                  }
                }
                
          }
          output.append("}");
          return output.toString();
          }
          catch(Exception e)
          {
            throw new RuntimeException(e);
          }
      }
    })).build();
  }






//Not required REST
  @GET
  @Produces("text/plain")
  @Path("/issue/field/{id}")
  public Response getAoFields(@PathParam("id") final String schemeID)
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              StringBuilder output = new StringBuilder();
              Scheme scheme = aoMappingService.getScheme(1);
              Set<NavigableField> customFields = ComponentAccessor.getFieldManager().getAllAvailableNavigableFields();
              output.append("{");
              for(FieldMapping fieldMap : scheme.getMappings())
              {
                if(fieldMap.getSyncType() != FieldMapping.SyncType.PULL)
                {
                  for(NavigableField field:customFields)
                  {
                    if(field.getName().equals(fieldMap.getJIRAFieldName()))
                    {
                      output.append(String.format("'%s':'%s',",field.getName(),fieldMap.getJIRAFieldName()));
                    }
                  }
                }
              }
              output.append("}");
              return output.toString();
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }
  
  @GET
  @Produces("text/plain")
  @Path("/issue/fields")
  public Response getAllAoFields()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              StringBuilder output = new StringBuilder();
              Scheme scheme = aoMappingService.getScheme(1);
              Set<NavigableField> customFields = ComponentAccessor.getFieldManager().getAllAvailableNavigableFields();
              for(NavigableField field : customFields)
              {
                output.append(field.getId());
                output.append("\n");
               }
              return output.toString();
            }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }


  @GET
  @Path("/synctransaction")
  @Produces("text/plain")
  public Response deleteSyncTransactions()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    transactionTemplate.execute(new TransactionCallback()
    {
        @Override
        public Object doInTransaction()
        {
        try
        {
          aoMappingService.deleteAllSyncTransaction();
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return "SUCCESS";
      }
    });
    return Response.noContent().build();
  }
  @GET
  @Path("/synctransactionlog")
  @Produces("text/plain")
  public Response deleteSyncTransactionLogs()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    transactionTemplate.execute(new TransactionCallback()
    {
        @Override
        public Object doInTransaction()
        {
        try
        {
          aoMappingService.deleteAllSyncTransactionLog();
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return "SUCCESS";
      }
    });
    return Response.noContent().build();
  }
  @GET
  @Path("/connectionconfig")
  @Produces("text/plain")
  public Response getConnectionConfig()
  {

    String username = userManager.getRemoteUsername();
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        
        StringBuilder output = new StringBuilder();
        try
        {
          ConnectionConfig connection = aoMappingService.getConnectionConfig();
          output.append(connection.getEndpointURL());
          output.append(",");
          output.append(connection.getUserName());
          output.append(",");
          output.append(connection.getPassword());
          output.append(",");
          output.append(connection.getAuthType());
          output.append(",");
          output.append(connection.getInterval());
          output.append(",");
          output.append(connection.getDuration());
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return output.toString();
      }
    })).build();
  }

}


