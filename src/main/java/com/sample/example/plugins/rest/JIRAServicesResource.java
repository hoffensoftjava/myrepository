package com.example.tutorial.plugins.admin.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.event.type.EventDispatchOption;


import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import net.java.ao.Query;
import java.util.Map;
import java.util.Enumeration;
import java.util.ArrayList; 
import java.util.List;
import java.util.*;
import java.util.Set;
import java.util.Comparator;

import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.XmlElement.XMLIssueField;
import com.example.tutorial.plugins.admin.util.FieldCollectionsUtils;
import com.atlassian.jira.issue.fields.*;
import com.example.tutorial.plugins.admin.util.*;

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;

@Path("/jira/issue")
@Named("JIRAServicesResource")
public class JIRAServicesResource
{
    private static final Logger log = LoggerFactory.getLogger(JIRAServicesResource.class);
  	@ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	  @ComponentImport
    private final TransactionTemplate transactionTemplate;
    private final FieldCollectionsUtils fieldCollectionsUtils;
    private IssueActionsUtils issueActionsUtils;

 	@Inject
  	public JIRAServicesResource(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,TransactionTemplate transactionTemplate,IssueActionsUtils issueActionsUtils,FieldCollectionsUtils fieldCollectionsUtils)
  	{
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.transactionTemplate = transactionTemplate;
        this.fieldCollectionsUtils = fieldCollectionsUtils;
        this.issueActionsUtils = issueActionsUtils;
  	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIssue()
	{

		String username = userManager.getRemoteUsername();
	  	if (username == null || !userManager.isSystemAdmin(username))
	  	{
	    	return Response.status(Status.UNAUTHORIZED).build();
	  	}

		return Response.ok(transactionTemplate.execute(new TransactionCallback()
		{
		    public Object doInTransaction()
		    {
		    try
        {

            List<Field> issueFields = fieldCollectionsUtils.getAvailableFields();
            List<XMLIssueField> fieldList = new ArrayList<XMLIssueField>();
            for(Field issueField : issueFields)
            {
                XMLIssueField field = new XMLIssueField();
                field.setName(issueField.getId());
                field.setLabel(issueField.getName());
                fieldList.add(field);
            }
            if(fieldList.size() > 0)
            {
                  Collections.sort(fieldList,XMLIssueField.TableNameComparator);
            }
            return fieldList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
	   	}
	  })).build();
	}

    @GET
    @Produces("text/plain")
    @Path("/{key}")
    @Deprecated
    public Response updateIssue(@PathParam("key") final String key)
    {

        log.info("Updating the issueFields");
        String username = userManager.getRemoteUsername();
        if (username == null || !userManager.isSystemAdmin(username))
        {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(new TransactionCallback()
        {
            public Object doInTransaction()
            {
            try
            {

                Issue issueObject = ComponentAccessor.getIssueManager().getIssueByCurrentKey(key);
                IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
                issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issueObject,"summary","updateFrom servicenow",changeHolder);
                issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issueObject,fieldCollectionsUtils.getFieldIdByName("SNID"),"false",changeHolder);
                ComponentAccessor.getIssueManager().updateIssue(currentAppUser,(MutableIssue)issueObject,EventDispatchOption.DO_NOT_DISPATCH,false);
                return String.format("%s SUCCESS %s",issueObject.getKey(),issueObject.getSummary());
            }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        }
      })).build();
    }

}


