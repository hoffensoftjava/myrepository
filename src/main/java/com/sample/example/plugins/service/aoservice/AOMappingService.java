package com.example.tutorial.plugins.ao.service;


import com.google.common.collect.Lists;
import net.java.ao.EntityManager;
import net.java.ao.Query;

import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.SyncTransaction;
import com.example.tutorial.plugins.admin.ao.SyncTransactionLog;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;

import com.atlassian.activeobjects.external.ActiveObjects;
import org.springframework.util.StringUtils;


import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Calendar;

import javax.annotation.concurrent.GuardedBy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.EnumSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

import static com.google.common.base.Preconditions.*;

@ExportAsService
@Component
public final class AOMappingService implements MappingService
{
	@ComponentImport
    protected final ActiveObjects activeObjects;
    private static final Logger log;
    static {
        log = LoggerFactory.getLogger(AOMappingService.class);
    }
    @Inject
	public AOMappingService(ActiveObjects activeObjects)
    {
        this.activeObjects = checkNotNull(activeObjects);
    }
    @Override
    public ActiveObjects getActiveObjects()
    {
        return this.activeObjects;
    }
    @Override
    public Scheme addScheme(String name,String jira_object_name,String sn_object_name)
    {
    	 final Scheme scheme;
    	 try
    	 {
    	 	scheme = activeObjects.create(Scheme.class);
    	 	scheme.setSchemeName(name);
    	 	scheme.setJIRAObjectName(jira_object_name);
    	 	scheme.setSNObjectName(sn_object_name);
    	 	scheme.save();
    	 	return scheme;
    	 }
    	 catch(Exception e )
    	 {
    	 	throw new RuntimeException(e);
    	 }
    }
    @Override
    public Scheme updateScheme(int schemeId,String name,String jira_object_name,String sn_object_name)
    {
         final Scheme scheme;
         try
         {
            scheme = getScheme(schemeId);
            scheme.setSchemeName(name);
            scheme.setJIRAObjectName(jira_object_name);
            scheme.setSNObjectName(sn_object_name);
            scheme.save();
            return scheme;
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }

    @Override
    public Iterable<Scheme> allSchemes()
    {
        try
        {
            final Query query = Query.select().order("id ASC");
            return Lists.newArrayList(activeObjects.find(Scheme.class, query));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    @Override
    public Scheme getScheme(int id)
    {
        final Scheme[] schems;
        try
        {
            final Query query = Query.select().where("id = ?", id);
            schems = activeObjects.find(Scheme.class, query);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return schems.length > 0 ? schems[0] : null;
    }
    public Scheme getSchemeForProject(String projectKey)
    {
        final Scheme[] schems;
        try
        {
            final Query query = Query.select()
                                .join(ProjectMapping.class,"PM.SCHEME_ID = SC.ID")
                                .alias(Scheme.class, "SC")
                                .alias(ProjectMapping.class, "PM")
                                .where("PM.PROJECT_NAME = ?", projectKey);

            schems = activeObjects.find(Scheme.class, query); 
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }   
        return schems.length > 0 ? schems[0] : null;
    }
    @Override
    public void deleteScheme(Scheme scheme)
    {
        try
        {
            for (FieldMapping fieldMapping : scheme.getMappings())
            {
                activeObjects.delete(fieldMapping);
            }
            for (ProjectMapping projectMapping : scheme.getProjectMappings())
            {
                activeObjects.delete(projectMapping);
            }
            activeObjects.delete(scheme);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

	@Override
	public FieldMapping addFieldMapping(Scheme scheme,String jira_field_name,String sn_field_name,FieldMapping.SyncType sync_Type,String snFieldType)
	{
		final FieldMapping fieldMapping;
    	 try
    	 {
    	 	fieldMapping = activeObjects.create(FieldMapping.class);
    	 	fieldMapping.setJIRAFieldName(jira_field_name);
    	 	fieldMapping.setSNFieldName(sn_field_name);
    	 	fieldMapping.setSyncType(sync_Type);
    	 	fieldMapping.setScheme(scheme);
            fieldMapping.setSNFieldType(snFieldType);
    	 	fieldMapping.save();
    	 	return fieldMapping;
    	 }
    	 catch(Exception e)
    	 {
    	 	throw new RuntimeException(e);
    	 }
	}

    @Override
    public FieldMapping getFieldMapping(int id)
    {
        final FieldMapping[] fieldMappings;
        try
        {
            final Query query = Query.select().where("id = ?", id);
            fieldMappings = activeObjects.find(FieldMapping.class, query);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return fieldMappings.length > 0 ? fieldMappings[0] : null;
    }

    @Override
    public void deleteFieldMapping(FieldMapping fieldMapping)
    {
        try
        {
            activeObjects.delete(fieldMapping);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void deleteFieldMappings(int schemeId)
    {
        try
        {
            log.info("######SchemeID2",schemeId);
            final Query query = Query.select()
                                .join(Scheme.class,"FM.SCHEME_ID = SC.ID")
                                .alias(Scheme.class, "SC")
                                .alias(FieldMapping.class, "FM")
                                .where("SC.ID = ?", schemeId);

            Iterable<FieldMapping> fieldMappings = Lists.newArrayList(activeObjects.find(FieldMapping.class, query)); 
            for(FieldMapping fieldMap : fieldMappings)
            {
                activeObjects.delete(fieldMap);    
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ProjectMapping addProjectMapping(Scheme scheme,String projectName,int interval,ProjectMapping.Duration duration)
    {
        final ProjectMapping projectMapping;
         try
         {
            projectMapping = activeObjects.create(ProjectMapping.class);
            projectMapping.setScheme(scheme);
            projectMapping.setProjectName(projectName);
            projectMapping.setInterval(interval);
            projectMapping.setDuration(duration);
            projectMapping.save();
            return projectMapping;
         }
         catch(Exception e)
         {
            throw new RuntimeException(e);
         }
    }

    @Override
    public Iterable<ProjectMapping> allProjectMappings()
    {
        try
        {
            final Query query = Query.select().order("PROJECT_NAME ASC");
            Iterable<ProjectMapping> proMap = Lists.newArrayList(activeObjects.find(ProjectMapping.class, query));            
            return proMap;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    @Override
    public ProjectMapping getProjectMapping(String projectName)
    {
        final ProjectMapping[] projectMappings;
        try
        {
            final Query query = Query.select().where("PROJECT_NAME = ?", projectName);
            projectMappings = activeObjects.find(ProjectMapping.class, query);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return projectMappings.length > 0 ? projectMappings[0] : null;
    }
    @Override
    public ProjectMapping getProjectMapping(int projectId)
    {
        final ProjectMapping[] projectMappings;
        try
        {
            final Query query = Query.select().where("id = ?", projectId);
            projectMappings = activeObjects.find(ProjectMapping.class, query);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return projectMappings.length > 0 ? projectMappings[0] : null;
    }
    @Override
    public void deleteProjectMapping(int projectId)
    {
        try
        {
            ProjectMapping proMap = getProjectMapping(projectId);
            activeObjects.delete(proMap);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void deleteALLProjectMappings()
    {
        try
        {
            final Query query = Query.select().order("id ASC");
            Iterable<ProjectMapping> projectMappings = Lists.newArrayList(activeObjects.find(ProjectMapping.class, query)); 
            for(ProjectMapping proMap : projectMappings)
            {
                activeObjects.delete(proMap);    
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void deleteProjectMappings(int schemeId)
    {
        try
        {
            final Query query = Query.select()
                                .join(Scheme.class,"PM.SCHEME_ID = SC.ID")
                                .alias(Scheme.class, "SC")
                                .alias(ProjectMapping.class, "PM")
                                .where("SC.ID = ?", schemeId);

            Iterable<ProjectMapping> projectMappings = Lists.newArrayList(activeObjects.find(ProjectMapping.class, query)); 
            for(ProjectMapping proMap : projectMappings)
            {
                activeObjects.delete(proMap);    
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public Map<String, Map<String,String>> fetchTablesOfProject() {
       try
        {
            
            Map<String,Map<String,String>> tableMap = new HashMap<String,Map<String,String>>();
                        final Query query = Query.select()
                                .join(ProjectMapping.class,"PM.SCHEME_ID = SC.ID")
                                .alias(Scheme.class, "SC")
                                .alias(ProjectMapping.class, "PM");
            Iterable<Scheme> schemes = Lists.newArrayList(activeObjects.find(Scheme.class, query)); 
            for(Scheme scheme : schemes)
            {
                Map<String,String> projectMap = new HashMap<String,String>();
                for(ProjectMapping project : scheme.getProjectMappings())
                {
                    projectMap.put(project.getProjectName(),project.getProjectName());
                }
                tableMap.put(scheme.getSNObjectName(),projectMap);
            }
            return tableMap;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        } 
    }

    @Override
    public SyncTransaction addSyncTransaction(Long jiraId,String snId,String projectKey)
    {
         final SyncTransaction syncTransaction;
         try
         {
            syncTransaction = activeObjects.create(SyncTransaction.class);
            syncTransaction.setJIRAId(jiraId);
            syncTransaction.setSNId(snId);
            syncTransaction.setProjectId(projectKey);
            syncTransaction.save();
            return syncTransaction;
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }

    public String getAllSyncTransaction()
    {
         try
         {
            StringBuffer sb = new StringBuffer();
            final Query query = Query.select();
            Iterable<SyncTransaction> syncTransactions = Lists.newArrayList(activeObjects.find(SyncTransaction.class, query)); 
            for(SyncTransaction syncTransaction : syncTransactions)
            {
                sb.append("JIRAId : "+ syncTransaction.getJIRAId());
                sb.append("SNId : "+ syncTransaction.getSNId());
                sb.append("ProjectId : "+ syncTransaction.getProjectId()); 
            }
            return sb.toString();
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }
    @Override
    public void deleteAllSyncTransaction()
    {
         try
         {
            final Query query = Query.select();
            Iterable<SyncTransaction> syncTransactions = Lists.newArrayList(activeObjects.find(SyncTransaction.class, query)); 
            for(SyncTransaction syncTransaction : syncTransactions)
            {
                activeObjects.delete(syncTransaction);
            }   
         }
         catch(Exception e)
         {
            throw new RuntimeException(e);
         }
    }
    @Override
    public void deleteSyncTransaction(Long jiraId)
    {
         final SyncTransaction[] syncTransaction;
         try
         {
            final Query query = Query.select().where("id = ?", jiraId);
            syncTransaction = activeObjects.find(SyncTransaction.class, query);
            if(syncTransaction.length > 0)
            {
                activeObjects.delete(syncTransaction[0]);
            }   
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }
    @Override
    public Set<Long> fetchSyncTransaction(Set<String> snIds,String projectKey)
    {
         Set<Long> jiraIds = new HashSet<Long>();
         try
         {
            StringBuffer sb = new StringBuffer(); 
            StringBuffer sb1 = new StringBuffer(); 
            for (String str : snIds) { 
             sb.append(str + ","); 
             sb1.append("?,");
            } 
            if (sb.length() > 0)
            {
                sb.deleteCharAt(sb.length() - 1);
                sb1.deleteCharAt(sb1.length() - 1);
            }
            final Query query = Query.select()
                                .where("SNID IN ("+ sb1.toString() +")",sb.toString())
                                .where("PROJECT_ID = ?",projectKey);
            Iterable<SyncTransaction> syncTransactions = Lists.newArrayList(activeObjects.find(SyncTransaction.class,query)); 
            for(SyncTransaction syncTransaction : syncTransactions)
            {
                log.info("@#$@#$@#$ getJIRAId {}",syncTransaction.getJIRAId());
                jiraIds.add(syncTransaction.getJIRAId());
            }
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }

        return jiraIds;
    }
    @Override
    public SyncTransactionLog addSyncTransactionLog(String content,String type,String action,Date updatedDate)
    {
        final SyncTransactionLog syncTransactionLog;
         try
         {
            syncTransactionLog = activeObjects.create(SyncTransactionLog.class);
            syncTransactionLog.setContent(content);
            syncTransactionLog.setSyncType(type);
            syncTransactionLog.setAction(action);
            syncTransactionLog.setUpdatedDate(updatedDate);
            syncTransactionLog.save();
            return syncTransactionLog;
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }
    @Override
    public Iterable<SyncTransactionLog> allSyncTransactionLog()
    {
        try
        {
            final Query query = Query.select().order("id DESC");
            return Lists.newArrayList(activeObjects.find(SyncTransactionLog.class, query));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    public void deleteAllSyncTransactionLog()
    {
        try
         {

            Iterable<SyncTransactionLog> syncTransactions = allSyncTransactionLog();
            for(SyncTransactionLog syncTransactionLog : syncTransactions)
            {
                activeObjects.delete(syncTransactionLog);
            }   
         }
         catch(Exception e)
         {
            throw new RuntimeException(e);
         }
    }

    public ConnectionConfig getConnectionConfig()
    {
        final ConnectionConfig[] connectionConfig;
        try
        {
            connectionConfig =  activeObjects.find(ConnectionConfig.class, Query.select().limit(1));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return connectionConfig.length > 0 ? connectionConfig[0] : null;
        
    }
    public ConnectionConfig submitConnectionConfig(String endPointUrl,String userName,String password,ConnectionConfig.AuthType authType,int interval,ConnectionConfig.Duration duration)
    {
        final ConnectionConfig connectionConfig;
         try
         {
            connectionConfig = getConnectionConfig() == null ? activeObjects.create(ConnectionConfig.class): getConnectionConfig();
            connectionConfig.setEndpointURL(endPointUrl);
            connectionConfig.setUserName(userName);
            connectionConfig.setPassword(password);
            connectionConfig.setAuthType(authType);
            connectionConfig.setInterval(interval);
            connectionConfig.setDuration(duration);
            connectionConfig.save();
            return connectionConfig;
         }
         catch(Exception e )
         {
            throw new RuntimeException(e);
         }
    }

}