package com.example.tutorial.plugins.ao.service;

import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.SyncTransaction;
import com.example.tutorial.plugins.admin.ao.SyncTransactionLog;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.*;
public interface MappingService
{
	public  ActiveObjects getActiveObjects();
	public Scheme addScheme(String name,String jira_object_name,String sn_object_name);
	public Iterable<Scheme> allSchemes();
	public Scheme getScheme(int id);
	public Scheme getSchemeForProject(String projectKey);
	public Scheme updateScheme(int schemeId,String name,String jira_object_name,String sn_object_name);
	public void deleteScheme(Scheme scheme);
	public FieldMapping addFieldMapping(Scheme scheme,String jira_field_name,String sn_field_name,FieldMapping.SyncType sync_Type,String snFieldType);
	public FieldMapping getFieldMapping(int id);
	public void deleteFieldMapping(FieldMapping fieldMapping);
	public void deleteFieldMappings(int schemeId);
	public ProjectMapping addProjectMapping(Scheme scheme,String projectName,int interval,ProjectMapping.Duration duration);
	public Iterable<ProjectMapping> allProjectMappings();
	public ProjectMapping getProjectMapping(String projectName);
	public ProjectMapping getProjectMapping(int projectId);
	public void deleteALLProjectMappings();
	public void deleteProjectMapping(int projectId);
	public void deleteProjectMappings(int schemeId);
	public SyncTransaction addSyncTransaction(Long jiraId,String snId,String projectKey);
	public void deleteSyncTransaction(Long jiraId);
	public Set<Long> fetchSyncTransaction(Set<String> snIds,String projectKey);
	public void deleteAllSyncTransaction();
	public SyncTransactionLog addSyncTransactionLog(String content,String type,String action,Date updatedDate);
	public Iterable<SyncTransactionLog> allSyncTransactionLog();
	public void deleteAllSyncTransactionLog();
	public ConnectionConfig getConnectionConfig();
	public ConnectionConfig submitConnectionConfig(String endPointUrl,String userName,String password,ConnectionConfig.AuthType authType,int interval,ConnectionConfig.Duration duration);

}
