
package com.example.tutorial.plugins;

import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.lifecycle.LifecycleManager;
import org.ofbiz.core.entity.GenericEntityException;
import com.atlassian.plugin.PluginException; 


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.GuardedBy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.EnumSet;
import java.util.Set;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.MutableIssue;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.ServiceNowClient;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.example.tutorial.plugins.exceptions.*;

@ExportAsService
@Component
@Named("CronServiceImpl")
public class CronServiceImpl implements CronService,LifecycleAware {

	 /* package */ static final String KEY = CronServiceImpl.class.getName() + ":instance";
	    private static final String JOB_NAME = CronServiceImpl.class.getName() + ":job";

	    static private final String PLUGIN_KEY="com.example.tutorial.plugins";

	      // provided by SAL
	    private AOMappingService aoMappingService;
	    private ServiceNowClient serviewNowClient;

	    private static final String SNID = "SNID";
	    private static final String SNSYNC = "SNSYNC";

    	@ComponentImport
    	private final PluginScheduler pluginScheduler;
    	@ComponentImport 
    	protected final CustomFieldManager customFieldManager;
    	@ComponentImport 
    	protected final FieldScreenManager fieldScreenManager;
    	private static final Logger log = LoggerFactory.getLogger(CronServiceImpl.class);

	    private long interval = 1*60*1000;      // default job interval (5 sec)
	    private List<Issue> issues;         // results of the last search
	    private Date lastRun = null;        // time when the last search returned



	    // enum LifecycleEvent
    	// {
     //    	AFTER_PROPERTIES_SET,
     //    	PLUGIN_ENABLED,
     //    	LIFECYCLE_AWARE_ON_START
    	// }
    	// @GuardedBy("this")
    	// private final Set<LifecycleEvent> lifecycleEvents = EnumSet.noneOf(LifecycleEvent.class);

	    @Inject
	    public CronServiceImpl(final PluginScheduler pluginScheduler,AOMappingService aoMappingService,
	    	ServiceNowClient serviewNowClient,final CustomFieldManager customFieldManager,
	    	final FieldScreenManager fieldScreenManager) {
	        this.pluginScheduler = pluginScheduler;
	        this.aoMappingService = aoMappingService;
	        this.serviewNowClient = serviewNowClient;
	        this.customFieldManager = customFieldManager;
	        this.fieldScreenManager = fieldScreenManager;
	    }
	    public void onStart() {
	    	setupCustomFields();
	    	this.interval = getIntervalMillisec();
	    	reschedule(interval);            
	    }
	    public void reschedule(long interval){
	        this.interval = interval;
	        if(interval > 0)
	        {
		        log.debug("interval: {}",interval);
		        pluginScheduler.scheduleJob(
		                JOB_NAME,                   // unique name of the job
		                CronServiceTask.class,     // class of the job
		                new HashMap<String,Object>() {{
		                    put(KEY, CronServiceImpl.this);
		                }},                         // data that needs to be passed to the job
		                new Date(),                 // the time the job is to start
		                interval);                  // interval between repeats, in milliseconds
		    }
	    }
	    private void setupCustomFields() 
	    {
	    	try
	    	{
		    	List<IssueType> issueTypes = new ArrayList<IssueType>();
		        issueTypes.add(null);

		        //Create a list of project contexts for which the custom field needs to be available
		        List<JiraContextNode> contexts = new ArrayList<JiraContextNode>();
		        contexts.add(GlobalIssueContext.getInstance());
		       

		        FieldScreen defaultScreen = fieldScreenManager.getFieldScreen(FieldScreen.DEFAULT_SCREEN_ID);
		        if (customFieldManager.getCustomFieldObjectByName(SNID) == null) {
			        CustomField snidField = this.customFieldManager.createCustomField(SNID, "Servicenow Identifier",
			            this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
			            this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
			            contexts, issueTypes);
			        
			        if (!defaultScreen.containsField(snidField.getId())) {
			            FieldScreenTab firstTab = defaultScreen.getTab(0);
			            firstTab.addFieldScreenLayoutItem(snidField.getId());
			        }
		     	}
		     	if (customFieldManager.getCustomFieldObjectByName(SNSYNC) == null) {
			        CustomField snsyncField = this.customFieldManager.createCustomField(SNSYNC, "Servicenow Sync Identifier",
			            this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
			            this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
			            contexts, issueTypes);
			        
			        if (!defaultScreen.containsField(snsyncField.getId())) {
			            FieldScreenTab firstTab = defaultScreen.getTab(0);
			            firstTab.addFieldScreenLayoutItem(snsyncField.getId());
			        }
		     	}
	     	}
	    	catch (GenericEntityException e) { 
            	log.error("Couldn't create risk Custom fields : " + e.getMessage(), e); 
            	throw new PluginException("GenericEntityException. Stopping plugin creation",e); 
        	} catch (NullPointerException e) { 
            	log.error("Couldn't create risk Custom fields:" + e.getMessage(), e); 
            	throw new PluginException("NullPointerException. Stopping plugin creation",e); 
       	 	}
	    }

	    private long getIntervalMillisec() {

	    	long intervalSeconds = 5*60*1000;
	    	ConnectionConfig connectionConfig = aoMappingService.getConnectionConfig();
	    	if(connectionConfig!=null)
	    	{
		    	if(connectionConfig.getDuration().toString() == "MINUTES")
		    	{
		    		intervalSeconds = (long)(connectionConfig.getInterval() * 60 * 1000);
		    	}
		    	else if(connectionConfig.getDuration().toString() == "HOURS")
		    	{
		    		intervalSeconds =  (long)(connectionConfig.getInterval() * 60 * 60 * 1000);
		    	}
		    	else if(connectionConfig.getDuration().toString() == "DAYS")
		    	{
		    		intervalSeconds =  (long)(connectionConfig.getInterval() * 24 * 60 * 60 * 1000);
		    	}
		    	else if(connectionConfig.getDuration().toString() == "WEEKS")
		    	{
		    		intervalSeconds =  (long)((connectionConfig.getInterval() * 7) * 24 * 60 * 60 * 1000);
		    	}
	    	}
	    	return intervalSeconds;
	    }

	    AOMappingService getAOMappingService()
	    {
	    	return aoMappingService;
	    }
	    ServiceNowClient getServiceNowClient()
	    {
	    	return serviewNowClient;
	    }

	    public Date getLastRun() {
        	return lastRun;
   		 }

		public long getInterval() {
        	return interval;
    	}	    
	    /* package */ void setIssues(List<Issue> issues) {
	        this.issues = issues;
	    }

	    /* package */ void setLastRun(Date lastRun) {
	        this.lastRun = lastRun;
	    }
}