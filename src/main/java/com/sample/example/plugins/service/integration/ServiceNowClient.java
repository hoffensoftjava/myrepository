package com.example.tutorial.plugins;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.inject.Inject;
import net.java.ao.EntityManager;
import net.java.ao.Query;

import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
import org.apache.commons.lang.StringUtils;
import java.text.SimpleDateFormat;
import com.example.tutorial.plugins.*;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.ao.service.*;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.ComponentManager;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.example.tutorial.plugins.exceptions.*;

@ExportAsService
@Component
public class ServiceNowClient
{
	private String changeRequestEndPoint = "/change_request";
	private String snBaseUrl = "";//ConfigHelper.getConnectionConfig().getEndpointURL();//https://dev10629.service-now.com/api/now/table/";
	private String schemasEndPoint = "";//https://dev10629.service-now.com/api/now/table/sys_db_object";
	private String tableColumnEndPoint = "";//https://dev10629.service-now.com/api/now/table/sys_dictionary";
	private String username = "";
	private String password = "";//Mahis!234
	private String contentType = "";
	
	public enum SERVICENOW_INTEGRATION {
        CREATE_ISSUE,
        UPDATE_ISSUE,
        FETCH_ALL_TABLES,
        FETCH_PARENT_TABLES,
        RETRIEVE_UPDATES,
        FETCH_SPECIAL_VALUES,
        FETCH_ALL_COLUMNS
    }

	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

	@ComponentImport
	private final RequestFactory requestFactory; 
	@ComponentImport
	private final PluginSettingsFactory pluginSettingsFactory;
	@ComponentImport
    protected final ActiveObjects activeObjects;
	private static final Logger log = LoggerFactory.getLogger(ServiceNowClient.class);
	

	@Inject
    public ServiceNowClient(final RequestFactory requestFactory,final PluginSettingsFactory pluginSettingsFactory,
    	final ActiveObjects activeObjects)
    {
         this.requestFactory = requestFactory;
         this.pluginSettingsFactory = pluginSettingsFactory;
         this.activeObjects = activeObjects;
         log.debug("this.activeObjects {}",this.activeObjects);
    }

    private void loadConfigSettings()
    {

	 ConnectionConfig connectionConfig = ConfigHelper.getConnectionConfig();
 	 contentType = ConfigHelper.CONTENT_TYPE;
     snBaseUrl = connectionConfig.getEndpointURL();
     if(snBaseUrl.substring(snBaseUrl.length() - 1) != "/")
     	snBaseUrl = snBaseUrl +"/";
     snBaseUrl = snBaseUrl.toLowerCase().contains(ConfigHelper.TABLE_API)?snBaseUrl : snBaseUrl + ConfigHelper.TABLE_API;
     schemasEndPoint = snBaseUrl.toLowerCase().contains(snBaseUrl+ConfigHelper.TABLE_NAME)? snBaseUrl : snBaseUrl + ConfigHelper.TABLE_NAME;
     tableColumnEndPoint = snBaseUrl.toLowerCase().contains(snBaseUrl + ConfigHelper.COLUMN_TABLE_NAME)? snBaseUrl : snBaseUrl + ConfigHelper.COLUMN_TABLE_NAME;
     username = connectionConfig.getUserName();
     password = connectionConfig.getPassword();

    // 	this.activeObjects.executeInTransaction(
    //     new TransactionCallback<Void>() {

    //       @Override
    //       public Void doInTransaction() {
				// ConnectionConfig[] connectionConfigs = activeObjects.find(ConnectionConfig.class,Query.select());
		  //        log.debug("this.connectionConfigs {}",connectionConfigs);
		  //        if(connectionConfigs.length > 0)
		  //        {
		  //        	 ConnectionConfig connectionConfig = connectionConfigs[0];
		  //        	 contentType = ConfigHelper.CONTENT_TYPE;
			 //         snBaseUrl = connectionConfig.getEndpointURL();
			 //         schemasEndPoint = snBaseUrl+ConfigHelper.TABLE_NAME;
			 //         tableColumnEndPoint = snBaseUrl+ConfigHelper.COLUMN_TABLE_NAME;
			 //         username = connectionConfig.getUserName();
			 //         password = connectionConfig.getPassword();
		  //    	 }
    //       return null;
    //       }
    //     }
    // );
    }
	public String saveIssueIntoServiceNow(String jsonString,String tableName) throws HSResponseException
	{	
		loadConfigSettings();
		String requestURL = snBaseUrl+tableName;
		Request request = requestFactory.createRequest(Request.MethodType.POST, requestURL);
		log.info("Endpoint for creating Issue : {}", snBaseUrl+tableName);
        request.setRequestBody(jsonString);
        String response = executeAPIRequest(request,SERVICENOW_INTEGRATION.CREATE_ISSUE,requestURL);
        log.debug("New Issue is created Successfully and the Response is : {}", response); 
        return response;
	}
	public String updateIssueIntoServiceNow(String jsonString,String tableName,String sysid) throws HSResponseException
	{	
		loadConfigSettings();
		String requestURL = snBaseUrl+tableName+"/"+sysid;
		Request request = requestFactory.createRequest(Request.MethodType.PUT, requestURL);
		log.info("Endpoint for updating Issue : {}", snBaseUrl+tableName+"/"+sysid);
        request.setRequestBody(jsonString);
        String response = executeAPIRequest(request,SERVICENOW_INTEGRATION.UPDATE_ISSUE,requestURL);
        log.debug("Exisitng Issue is updated Successfully and the Response is : {}", response); 
        return response;
	}
	public void checkValidConnection() throws HSResponseException,HSJSONNotFormattedException
	{
		String jsonRequest = "";
        try
        {
           jsonRequest = fetchAllTables(); 
        }
        catch(HSResponseException e)
        {
            log.error(e.getErrorString());
            throw new HSResponseException(e.getErrorString(),e.getErrorString());
        }
        try
        {
           
            JSONArray jsonResult = new JSONObject(jsonRequest).getJSONArray("result");
            for (int i = 0; i < jsonResult.length(); i++)
            {
                JSONObject tableJSON = jsonResult.getJSONObject(i);
                String name = tableJSON.getString("name");
                String label = tableJSON.getString("label");
                String sysid = tableJSON.getString("sys_id");
                String link  = "";
                String value = "";
                if(tableJSON.has("sys_package") && !tableJSON.isNull("sys_package") && !tableJSON.getString("sys_package").isEmpty())
                {
                    JSONObject jsonSysPackage = tableJSON.getJSONObject("sys_package");
                    link = jsonSysPackage.getString("link");
                    value = jsonSysPackage.getString("value");
                }
            }
        }
        catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is not in expected format in Connection Testing(){}",jsonRequest);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is not in expected format in ConnectionTesting() function"+jsonRequest,jsonRequest);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
	} 
	public String fetchAllTables() throws HSResponseException
	{	
		loadConfigSettings();
		String requestURL="";
		try
		{
			requestURL = String.format("sysparm_query=%s",URLEncoder.encode("is_extendable!=false","UTF-8"));		
		}
		 catch (UnsupportedEncodingException e) 
   		{ 
    		log.error("fetchAllTables - Error generating UTF8 encoding.", e); 
   		}
		 
		requestURL = schemasEndPoint +"?"+ requestURL;
		log.info("Endpoint for fetchAllTables : {}", requestURL);
		//Request request = requestFactory.createRequest(Request.MethodType.GET, requestURL);
        String response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_ALL_TABLES,requestURL);
        return response;
	}

	public String fetchAllParentTables(String tableName) throws HSJSONNotFormattedException,HSResponseException
	{
		loadConfigSettings();
		Set<String> tableList = new HashSet<String>();
		while(!tableName.isEmpty())
		{
			String name = fetchParentTable(tableName);
			if(!name.isEmpty())
			{
				tableList.add(name.split(",")[0]);
				tableName = name.split(",")[0];
			}
			else
			{
				tableName = "";
			}
		}
		return StringUtils.join(tableList, ',');
	}

	public List<NameValueMapper> fetchAllParentTablesFoTest(String tableName) throws HSJSONNotFormattedException,HSResponseException
	{
		loadConfigSettings();
		List<NameValueMapper> tableList = new ArrayList<NameValueMapper>();
		while(!tableName.isEmpty())
		{
			String name = fetchParentTable(tableName);
			if(!name.isEmpty())
			{
				tableList.add(new NameValueFactory(name.split(",")[0],name.split(",")[1]));
				tableName = name.split(",")[0];
			}
			else
			{
				tableName = "";
			}
		}
		return tableList;
	}

	public String fetchParentTable(String tableName) throws HSJSONNotFormattedException,HSResponseException
	{	
		loadConfigSettings();
		String requestURL="";
		try
		{
			requestURL = String.format("sysparm_query=name=%s",URLEncoder.encode(tableName,"UTF-8"));		
		}
		catch (UnsupportedEncodingException e) 
   		{ 
    		log.error("fetchAllTables - Error generating UTF8 encoding.", e); 
   		}
   		String response = "";
   		try
   		{
			requestURL = schemasEndPoint +"?"+ requestURL;
			//Request request = requestFactory.createRequest(Request.MethodType.GET, requestURL);
	        response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_PARENT_TABLES,requestURL);
	        JSONObject tableJSON = new JSONObject(response).getJSONArray("result").getJSONObject(0);
	        String parentTableName = "";
	        if(tableJSON.has("super_class") && !tableJSON.isNull("super_class") && !tableJSON.getString("super_class").isEmpty())
	        {
	            JSONObject jsonSysPackage = tableJSON.getJSONObject("super_class");
	            String link  = jsonSysPackage.getString("link");
	            //request = requestFactory.createRequest(Request.MethodType.GET, link);
	            response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_PARENT_TABLES,link);
	            tableJSON = new JSONObject(response).getJSONObject("result");
	            if(tableJSON.has("name") && !tableJSON.isNull("name") && !tableJSON.getString("name").isEmpty())
	        	{
	        			parentTableName = String.format("%s,%s",tableJSON.getString("name"),tableJSON.getString("label"));
	        	}
	        }
	        return parentTableName;
   		}
   		catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is in expected format in fetchParentTable(){}",response);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is in expected format in fetchParentTable() function",response);
        }
	    catch (Exception e)
	    {
	    	throw new RuntimeException(e);
	    }
	}

	public String retrieveUpdatesFromServicenNow(String tableName,Date lastRun,String sysparam_fields) throws HSResponseException
	{
		loadConfigSettings();
		String requestURL="";
   		try
   		//https://dev10629.service-now.com/api/now/table/change_request?sysparm_query=sys_updated_onONToday@javascript:gs.daysAgoStart(0)@javascript:gs.daysAgoEnd(0)^sysparm_fields=short_description
   		{
   			DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
	        TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
   			String dateString = DATE_FORMAT.format(lastRun);
			String timeString = TIME_FORMAT.format(lastRun);
			requestURL = String.format("sysparm_query=sys_updated_on%s('%s','%s')&sysparm_fields=%s",URLEncoder.encode(">=javascript:gs.dateGenerate","UTF-8"),URLEncoder.encode(dateString,"UTF-8"),URLEncoder.encode(timeString,"UTF-8"),URLEncoder.encode(sysparam_fields,"UTF-8"));		
			requestURL = snBaseUrl+tableName +"?"+ requestURL;
			log.debug("Endpoint for retrieveUpdatesFromServicenNow : {} ",requestURL);
			//Request request = requestFactory.createRequest(Request.MethodType.GET, requestURL);
	        String response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.RETRIEVE_UPDATES,requestURL);
	        return response;
   		}
	    catch (Exception e)
	    {
	    	throw new RuntimeException(e);
	    }
	}

	public String fetchSpecialFieldValues(String sys_Id,String fieldName,String apiURL,Date lastRun) throws HSResponseException
	{
		loadConfigSettings();
		String requestURL="";
		try
		{
			DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
	        TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
   			String dateString = DATE_FORMAT.format(lastRun);
			String timeString = TIME_FORMAT.format(lastRun);
		 	requestURL = String.format(apiURL,URLEncoder.encode(sys_Id,"UTF-8"),URLEncoder.encode("^","UTF-8"),URLEncoder.encode(fieldName,"UTF-8"),URLEncoder.encode("^","UTF-8"),URLEncoder.encode(">=javascript:gs.dateGenerate","UTF-8"),URLEncoder.encode(dateString,"UTF-8"),URLEncoder.encode(timeString,"UTF-8"));	
		 	requestURL = snBaseUrl + requestURL;
		}
		 catch (UnsupportedEncodingException e) 
   		{ 
    		log.error("fetchSpecialFieldValues - Error generating UTF8 encoding.", e); 
   		}
   		log.debug("Endpoint for fetchSpecialFieldValues : {} ",requestURL);
		//Request request = requestFactory.createRequest(Request.MethodType.GET, requestURL);
        String response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_SPECIAL_VALUES,requestURL);
        return response;
	}

	public String fetchAllTableColumnNames(String tableName) throws HSResponseException
	{	
		loadConfigSettings();
		String requestURL="";
		//requestURL = "sysparm_limit=1";
		
		try
		{
		 	tableName = "nameIN"+tableName+"";
		 	requestURL = String.format("sysparm_query=%s",URLEncoder.encode(tableName,"UTF-8"));		
		}
		 catch (UnsupportedEncodingException e) 
   		{ 
    		log.error("fetchAllTableColumnNames - Error generating UTF8 encoding.", e); 
   		}
		 
		requestURL = tableColumnEndPoint +"?"+ requestURL;
		log.debug("Endpoint for fetchAllTableColumnNames : {} ",requestURL);
        String response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_ALL_COLUMNS,requestURL);
        return response;
	}

	public String getBasicAuthString()
	{
		String authString = username + ":" + password;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		log.debug("Basic Authstring Basic {}",authStringEnc);

        return "Basic " + authStringEnc;
	}

	public String createChangeRequest(Issue currentIssue) throws ResponseException
	{
		loadConfigSettings();
		String issueKey = currentIssue.getKey();
		Request request = requestFactory.createRequest(Request.MethodType.POST, changeRequestEndPoint);
        request.setHeader("Content-Type", contentType);
        request.setHeader("Authorization", getBasicAuthString());
        String requestString = String.format("{'short_description':'Change Request Created from JIRA Ticket : %s'}",issueKey);
        request.setRequestBody(requestString);
        String response = request.execute();
        return response;
	}
	private String executeAPIRequest(Request.MethodType methodType,SERVICENOW_INTEGRATION integration_Type,String requestUrl) throws HSResponseException
	{
		Request request = requestFactory.createRequest(methodType, requestUrl);
		return executeAPIRequest(request,integration_Type,requestUrl);
	}
	private String executeAPIRequest(Request request,SERVICENOW_INTEGRATION integration_Type,String requestUrl) throws HSResponseException
	{
		String responseJSON = "";
		SNIntegratonResponseHandler handler = new SNIntegratonResponseHandler();
		String requestDetails = String.format("EndpointURL : %s; Integration Type : %s;",requestUrl,integration_Type);	
		try
		{
		request.setHeader("Content-Type", contentType);
        request.setHeader("Authorization", getBasicAuthString());
        request.setHeader("Accept-Charset", "UTF-8");
        request.execute(handler);
        if(handler.isSuccessful())
        {
        	responseJSON = handler.getResponse();
        }
        else
        {
         String exceptionString = String.format("Request Failed; Reason: %d-%s;Details: %s ",
                        handler.getStatusCode(),handler.getStatusText(),requestDetails);
         log.error(exceptionString +"{}",responseJSON);
         throw new HSResponseException("Exception in executing the API",exceptionString);
         
    	}
        log.debug("response: {} : details : {} ",responseJSON,requestDetails);
        return responseJSON;
        }
		catch(ResponseException e)
		{
			String exceptionString = String.format("Request Failed; Reason: %d-%s;Details: %s ",
                        handler.getStatusCode(),handler.getStatusText(),requestDetails);
         log.error(exceptionString +"{}",responseJSON);
         throw new HSResponseException("Exception in executing the API",exceptionString);
		}
	}
}