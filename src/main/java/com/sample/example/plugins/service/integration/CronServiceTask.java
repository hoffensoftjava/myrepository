
package com.example.tutorial.plugins;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.annotation.concurrent.GuardedBy;
import javax.inject.Inject;
import javax.inject.Named;

import java.util.EnumSet;
import java.util.Set;
import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.*;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.user.UserManager;


import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.SyncTransaction;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.util.*;
import com.example.tutorial.plugins.*;
import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.ao.SyncTransactionLog;
import com.example.tutorial.plugins.exceptions.*;


@Component
public class CronServiceTask implements PluginJob{

	private static final Logger log = LoggerFactory.getLogger(CreateChangeRequestActionSupport.class);
	private ApplicationUser currentAppUser;
	private String SN_COMMENT_SUFFIX = ConfigHelper.SN_COMMENT_SUFFIX;
	private String JIRA_COMMENT_SUFFIX= ConfigHelper.JIRA_COMMENT_SUFFIX;
	private final String SYNC_FIELD_NAME = "SNSYNC";
	private final String SN_IDENTIFIER_NAME = "SNID";

	    /**
	     * Executes this job.
	     *
	     * @param jobDataMap any data the job needs to execute. Changes to this data will be remembered between executions.
	     */
	    @Override
	    public void execute(Map<String, Object> jobDataMap) {

			final CronServiceImpl serviceImpl = (CronServiceImpl)jobDataMap.get(CronServiceImpl.KEY);
			assert serviceImpl != null;
			snIntegration(serviceImpl);
			serviceImpl.setLastRun(new Date());
        }

        public void snIntegration(final CronServiceImpl serviceImpl)  
        {	
        	ServiceNowFieldUtils serviceNowFieldUtils = StaticHelper.getServiceNowFieldUtils();
	        ServiceNowClient serviceNowClient = StaticHelper.getServiceNowClient();
	        IssueManager issueManager= StaticHelper.getIssueManager();
	        CommentManager commentManager = StaticHelper.getCommentManager();
	        RequestFactory requestFactory = StaticHelper.getRequestFactory();
			JiraAuthenticationContext jiraAuthContext = StaticHelper.getJiraAuthContext();
			FieldCollectionsUtils fieldCollectionsUtils = StaticHelper.getFieldCollectionsUtils();
			IssueActionsUtils issueActionsUtils = StaticHelper.getIssueActionsUtils();

			AOMappingService aoMappingService = serviceImpl.getAOMappingService();
        	try
        	{
	        	Iterable<Scheme> schemes = aoMappingService.allSchemes();
	        	Set<Long> issueIds = new HashSet<Long>();
	        	Map<String,String> specialFieldMap = new HashMap<String,String>();
        		Map<String,String> updateNormalFieldResponseMap = new HashMap<String,String>();
        		Map<String,String> updateSpecialFieldResponseMap = new HashMap<String,String>();
        		Date currentDate = new Date();
        		//currentDate.setTime(currentDate.getTime() - 2 * 24 * 3600 * 1000);
        		//currentDate.setTime(currentDate.getTime() - 1 * 1 * 600 * 1000);
        		currentDate = serviceImpl.getLastRun() == null ? currentDate : serviceImpl.getLastRun();
        		log.info("currentDate {}",currentDate);
        		for(Scheme scheme : schemes) {
        			StringBuilder sysparam_Field = new StringBuilder();
        			sysparam_Field.append("sys_id,");//Default Value
	        		String snTableName = scheme.getSNObjectName();
	        		for(FieldMapping field : scheme.getMappings())
		        	{
		        		if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()) != null)
		        		{
		        			if(specialFieldMap.get(field.getSNFieldName()) == null)
		        			{
		        				specialFieldMap.put(field.getSNFieldName(),serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()));
		        			}
		        		}
		        		else
		        		{
		        			sysparam_Field.append(field.getSNFieldName());
		        			sysparam_Field.append(",");
		        		}
		        	}
	        		String snUpdateResponse = serviceNowClient.retrieveUpdatesFromServicenNow(snTableName,currentDate,sysparam_Field.toString());
	        		log.info("snUpdateResponse {}",snUpdateResponse);
	        		if(snUpdateResponse!=null)
	        		{
		        		fetchSysIds(snUpdateResponse,updateNormalFieldResponseMap);
		        		for(ProjectMapping proMap : scheme.getProjectMappings())
		        		{
		        			Set<Long> ids = aoMappingService.fetchSyncTransaction(updateNormalFieldResponseMap.keySet(),proMap.getProjectName()); 	
		        			issueIds.addAll(ids);
		        		}

			        	
		        	}
	        	}
	        	if(!specialFieldMap.isEmpty())
        		{
	        		for(String sysId:updateNormalFieldResponseMap.keySet())
					{
						for(String specialField : specialFieldMap.keySet())
						{
							getSpecialFieldValues(sysId,specialField,specialFieldMap.get(specialField),updateSpecialFieldResponseMap,currentDate,serviceNowClient);	
						}
					}
        		}
        		log.info("issueIds {}",issueIds);
        		if(issueIds.size() > 0)
        		{
        			Set<String> responseString = new HashSet<String>();
        			List<Issue> issueCollections = issueManager.getIssueObjects(issueIds);
        			for(Issue issue : issueCollections)
					{
						String snID = issueActionsUtils.getFieldStringValue(issue,fieldCollectionsUtils.getFieldIdByName(SN_IDENTIFIER_NAME));
						String updateNormalFieldResponse = updateNormalFieldResponseMap.get(snID);
						String updateSpecialFieldResponse = updateSpecialFieldResponseMap.get(snID);
						updateSpecialValueIntoIssue(issue,updateNormalFieldResponse,updateSpecialFieldResponse,aoMappingService);
						issueManager.updateIssue(currentAppUser,(MutableIssue)issue,EventDispatchOption.DO_NOT_DISPATCH,false);
						log.debug("updateNormalFieldResponse {} updateSpecialFieldResponse {}",updateNormalFieldResponse,updateSpecialFieldResponse);
						if(updateNormalFieldResponse != null || updateSpecialFieldResponse != null)
						{
							responseString.add(String.format("{IssueId:%s,Normal Field response: %s , SpecialField Response : %s}",String.valueOf(issue.getId()),updateNormalFieldResponse,updateSpecialFieldResponse));
						}
					}
					if(responseString.size()>0)
					{
						String content = String.format("%s",responseString);
						aoMappingService.addSyncTransactionLog(content,"PULL","UPDATE",new Date());
					}
				}
        	}
        	catch(HSJSONNotFormattedException e)
        	{
            	log.error(e.getMessage()+e.getJSONString());
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

        private void getSpecialFieldValues(String sysId,String fieldName,String endPointAPI,Map<String,String> updateSpecialFieldResponseMap,Date currentDate,ServiceNowClient serviceNowClient) 
        {
        	String specialFieldResponse = "";
        	try
        	{
	        	specialFieldResponse = serviceNowClient.fetchSpecialFieldValues(sysId,fieldName,endPointAPI,currentDate);
	        	if(updateSpecialFieldResponseMap.get(sysId) == null)
	        	{
	        		updateSpecialFieldResponseMap.put(sysId,specialFieldResponse);
	        	}
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}

        }

        private void updateSpecialValueIntoIssue(Issue issue,String updateNormalFieldResponse,String updateSpecialFieldResponse,AOMappingService aoMappingService ) throws HSJSONNotFormattedException
        {
        	try
        	{
        		//= serviceImpl.getAOMappingService();
	        	ServiceNowFieldUtils serviceNowFieldUtils = StaticHelper.getServiceNowFieldUtils();
				FieldCollectionsUtils fieldCollectionsUtils = StaticHelper.getFieldCollectionsUtils();
	        	IssueActionsUtils issueActionsUtils = StaticHelper.getIssueActionsUtils();
	        	Scheme scheme = aoMappingService.getSchemeForProject(issue.getProjectObject().getKey());
	        	IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
	        	for(FieldMapping fieldMapping : scheme.getMappings())
	        	{
	        		String snFieldValue = "";
	        		String jiraFieldKey = fieldMapping.getJIRAFieldName();
					String snFieldKey = fieldMapping.getSNFieldName();
					if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(fieldMapping.getSNFieldType().toUpperCase())!= null)
					{
						if(updateSpecialFieldResponse!=null)
						{
							JSONArray jsonResult = new JSONObject(updateSpecialFieldResponse).getJSONArray("result");
							for (int i = 0; i < jsonResult.length(); i++)
							{
								JSONObject tableJSON = jsonResult.getJSONObject(i);
								if(tableJSON.has("value") && !tableJSON.getString("value").isEmpty())
								{
									snFieldValue = tableJSON.getString("value");
									String createdBy = tableJSON.getString("sys_created_by");
									String createdOn = tableJSON.getString("sys_created_on");
									if(!snFieldValue.contains(JIRA_COMMENT_SUFFIX))
									{
										snFieldValue = String.format(SN_COMMENT_SUFFIX,snFieldValue,createdBy,createdOn);
										issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(jiraFieldKey),snFieldValue,changeHolder);	
									}
								}
							}
						}
					}
					else
					{
						if(updateNormalFieldResponse !=null)
						{
						 	JSONObject jsonString = new JSONObject(updateNormalFieldResponse);
						 	snFieldValue= jsonString.getString(snFieldKey);	
						 	issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(jiraFieldKey),snFieldValue,changeHolder);							
						}
					}
	        	}
	        	log.debug("Updating Identifier Field using setFieldValue {}",scheme);
        	}
        	catch(JSONException e)
        	{
            	log.error("Looks like the Servicenow endpoint response is in expected format in updateSpecialValueIntoIssue(){}",updateNormalFieldResponse + updateSpecialFieldResponse);
            	throw new HSJSONNotFormattedException("Servicenow endpoint response is in expected format in updateSpecialValueIntoIssue() function",updateNormalFieldResponse + updateSpecialFieldResponse);
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

        private void fetchSysIds(String snUpdateResponse,Map<String,String> updateNormalFieldResponseMap) throws HSJSONNotFormattedException 
        {
        	try
        	{
        	JSONArray jsonResult = new JSONObject(snUpdateResponse).getJSONArray("result");
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		if(updateNormalFieldResponseMap.get(tableJSON.getString("sys_id")) == null)
	            		{
	            			updateNormalFieldResponseMap.put(tableJSON.getString("sys_id"),tableJSON.toString());
	            		}
	            	}
            	}
            }
            catch(JSONException e)
        	{
            	log.error("Looks like the Servicenow endpoint response is in expected format in fetchSysIds(){}",snUpdateResponse);
            	throw new HSJSONNotFormattedException("Servicenow endpoint response is in expected format in fetchSysIds() function",snUpdateResponse);
        	}
        	catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }


    private Set<String> fetchSysIds(String snUpdateResponse) throws HSJSONNotFormattedException
        {
        	try
        	{
        	JSONArray jsonResult = new JSONObject(snUpdateResponse).getJSONArray("result");
	            Set<String> tableSets = new HashSet<String>();
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		tableSets.add(tableJSON.getString("sys_id"));
	            	}
            	}
            	return tableSets;
         }
        catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is in expected format in fetchSysIds(){}",snUpdateResponse);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is in expected format in fetchSysIds() function",snUpdateResponse);
        }
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }
 

        /*
        public void snIntegration(Iterable<Scheme> schemes,final CronServiceImpl serviceImpl)  
        {	
        	try
        	{
        	ServiceNowClient serviceNowClient = serviceImpl.getServiceNowClient();
        	AOMappingService aoMappingService = serviceImpl.getAOMappingService();
        	FieldCollectionsUtils fieldCollectionsUtils = StaticHelper.getFieldCollectionsUtils();
        	IssueActionsUtils issueActionsUtils = StaticHelper.getIssueActionsUtils();
        	IssueManager issueManager = StaticHelper.getIssueManager();
        	Map<String,String> specialFieldMap = new HashMap<String,String>();
        	Map<String,String> updateNormalFieldResponseMap = new HashMap<String,String>();
        	Map<String,String> updateSpecialFieldResponseMap = new HashMap<String,String>();
        	Set<Long> issueIds = new HashSet<Long>();
        	for(Scheme scheme : schemes) {
          		
        		String snTableName = scheme.getSNObjectName();
        		String snUpdateResponse = serviceNowClient.retrieveUpdatesFromServicenNow(snTableName,serviceImpl.getLastRun());
        		fetchSysIds(snUpdateResponse,updateNormalFieldResponseMap);
        		for(ProjectMapping proMap : scheme.getProjectMappings())
        		{
        			Set<Long> ids = aoMappingService.fetchSyncTransaction(updateNormalFieldResponseMap.keySet(),proMap.getProjectName()); 	
        			issueIds.addAll(ids);
        		}
        		fetchSchemeWithSpecialFields(scheme.getMappings(),specialFieldMap);
        	}
        	
        	if(!specialFieldMap.isEmpty())
        	{
        		for(String sysId:updateNormalFieldResponseMap.keySet())
				{
					for(String specialField : specialFieldMap.keySet())
					{
						getSpecialFieldValues(sysId,specialField,specialFieldMap.get(specialField),updateSpecialFieldResponseMap,serviceImpl);	
					}
					
				}
        	}

			List<Issue> issueCollections = issueManager.getIssueObjects(issueIds);
        	for(Issue issue : issueCollections)
			{
				String snID = issueActionsUtils.getFieldStringValue(issue,fieldCollectionsUtils.getFieldIdByName("SNID"));
				String updateNormalFieldResponse = updateNormalFieldResponseMap.get(snID);
				String updateSpecialFieldResponse = updateSpecialFieldResponseMap.get(snID);
				updateSpecialValueIntoIssue(issue,updateNormalFieldResponse,updateSpecialFieldResponse,serviceImpl);
			}
        }
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

        private Map<String,String> fetchSchemeWithSpecialFields(FieldMapping[] fieldMappings,Map<String,String> specialFieldMap)
        {
        	try
        	{
	        	ServiceNowFieldUtils serviceNowFieldUtils = StaticHelper.getServiceNowFieldUtils();
	        	for(FieldMapping field:fieldMappings)
	        	{
	        		if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()) != null)
	        		{
	        			if(specialFieldMap.get(field.getSNFieldName()) == null)
	        			{
	        				specialFieldMap.put(field.getSNFieldName(),serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(field.getSNFieldType().toUpperCase()));
	        			}
	        		}
	        	}
	        	return specialFieldMap;
        	}
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }

        private void getSpecialFieldValues(String sysId,String fieldName,String endPointAPI,Map<String,String> updateSpecialFieldResponseMap,final CronServiceImpl serviceImpl)
        {
        	try
        	{
	        	ServiceNowClient serviceNowClient = serviceImpl.getServiceNowClient();
	        	String specialFieldResponse = serviceNowClient.fetchSpecialFieldValues(sysId,fieldName,endPointAPI,serviceImpl.getLastRun());
	        	if(updateSpecialFieldResponseMap.get(sysId) == null)
	        	{
	        		updateSpecialFieldResponseMap.put(sysId,specialFieldResponse);
	        	}
        	}
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}

        }

        private void updateSpecialValueIntoIssue(Issue issue,String updateNormalFieldResponse,String updateSpecialFieldResponse,final CronServiceImpl serviceImpl)
        {
        	try
        	{
	        	AOMappingService aoMappingService = serviceImpl.getAOMappingService();
	        	ServiceNowFieldUtils serviceNowFieldUtils = StaticHelper.getServiceNowFieldUtils();
				FieldCollectionsUtils fieldCollectionsUtils = StaticHelper.getFieldCollectionsUtils();
	        	IssueActionsUtils issueActionsUtils = StaticHelper.getIssueActionsUtils();
	        	Scheme scheme = aoMappingService.getSchemeForProject(issue.getProjectObject().getKey());
	        	IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
	        	for(FieldMapping fieldMapping : scheme.getMappings())
	        	{
	        		String snFieldValue = "";
	        		String jiraFieldKey = fieldMapping.getJIRAFieldName();
					String snFieldKey = fieldMapping.getSNFieldName();
					if(serviceNowFieldUtils.getSpecialSNFieldWithAPI().get(fieldMapping.getSNFieldType().toUpperCase())!= null)
					{
						JSONObject jsonString = new JSONObject(updateSpecialFieldResponse);
						snFieldValue = jsonString.toString(); // For timebeing the response will be multiple result so removed .getString("value");
					}
					else
					{
						JSONObject jsonString = new JSONObject(updateNormalFieldResponse);
						 snFieldValue= jsonString.getString(snFieldKey);
					}

	        		issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issue,fieldCollectionsUtils.getFieldIdByName(jiraFieldKey),snFieldValue,changeHolder);	
	        	}
        	}
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }


        private void fetchSysIds(String snUpdateResponse,Map<String,String> updateNormalFieldResponseMap) 
        {
        	try
        	{
        	JSONArray jsonResult = new JSONObject(snUpdateResponse).getJSONArray("result");
	            for (int i = 0; i < jsonResult.length(); i++)
	            {
	            	JSONObject tableJSON = jsonResult.getJSONObject(i);
	            	if(tableJSON.has("sys_id") && !tableJSON.getString("sys_id").isEmpty())
	            	{
	            		if(updateNormalFieldResponseMap.get(tableJSON.getString("sys_id")) == null)
	            		{
	            			updateNormalFieldResponseMap.put(tableJSON.getString("sys_id"),tableJSON.toString());
	            		}
	            	}
            	}
            }
        catch(Exception e)
        	{
            	throw new RuntimeException(e);
        	}
        }
	

	@Override
	    public void execute(Map<String, Object> jobDataMap) {
			
			IssueManager issueManager = StaticHelper.getIssueManager();
			CommentManager commentManager = StaticHelper.getCommentManager();
			RequestFactory requestFactory = StaticHelper.getRequestFactory();
			JiraAuthenticationContext jiraAuthContext = StaticHelper.getJiraAuthContext();
			
			// * Fetching the value from global config settings to check whether Schedule JOB is ENABLED or NOT*
			PluginSettingsFactory pluginSettingsFactory = StaticHelper.getPluginSettingsFactory();
			String PLUGIN_STORAGE_KEY = "com.example.tutorial.plugins.auth";
			PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
			String isSerive = (String) pluginSettings.get(PLUGIN_STORAGE_KEY + ".isServiceEnabled");
			log.info("#########Is Service Enabled Feature1 {}",isSerive);
			if(pluginSettings.get(PLUGIN_STORAGE_KEY + ".isServiceEnabled") != null)
			{
				log.info("######### Is Service Enabled Feature2 {}",Boolean.parseBoolean(isSerive));
				boolean isServiceEnabled = Boolean.parseBoolean(isSerive);
				if(isServiceEnabled)
				{
					currentAppUser = jiraAuthContext.getUser();
					serviewNowClient = new ServiceNowClient(requestFactory,pluginSettingsFactory);					
					MutableIssue mutableissue = issueManager.getIssueObject(id);
	           		String body = String.format("Change Request Created in Service now. %1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL%1$tz",new Date());
	        		commentManager.create(mutableissue,this.currentAppUser,body,true);
        		}
        	else
        		{
        			log.info("############This Service is not enabled to do any operation#############");
        		}
        	}
        }


	    	Email email = new Email("denniselite@live.com");
	    	email.setBody("Some body...");
	    	email.setMimeType("text/plain");
	    	email.setSubject("Some Subject");
	    	SingleMailQueueItem item = new SingleMailQueueItem(email);
	    	final MailServiceImpl mailService = new MailServiceImpl(taskManager);
	    	mailService.sendEmail(item);
	        final CronServiceImpl monitor = (CronServiceImpl)jobDataMap.get(CronServiceImpl.KEY);
	        assert monitor != null;
//	        try {
	        	final List<Issue> issues = this.getIssues();
//	            final Twitter twitter = new Twitter();
//	            monitor.setTweets(twitter.search(new Query(monitor.getQuery())).getTweets());
	        	monitor.setIssues(issues);
	            monitor.setLastRun(new Date());
//	        } catch (TwitterException te) {
//	            logger.error("Error talking to Twitter: " + te.getMessage(), te);
//	        }

	    private List<Issue> getIssues() {
	    	User user = ComponentAccessor.getJiraAuthenticationContext().getUser().getDirectoryUser();;
			// search issues
			// The search interface requires JQL clause... so let's build one
			JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
			// Our JQL clause is simple project="TUTORIAL"
			com.atlassian.query.Query query = jqlClauseBuilder.project("TestProject").buildQuery();
			// A page filter is used to provide pagination. Let's use an unlimited filter to
			// to bypass pagination.
			PagerFilter pagerFilter = PagerFilter.getUnlimitedFilter();
			com.atlassian.jira.issue.search.SearchResults searchResults = null;
			try {
				// Perform search results
				searchResults = searchService.search(user, query, pagerFilter);
			} catch (SearchException e) {
				e.printStackTrace();
			}
			// return the results
			return searchResults.getIssues();
	    }
*/

}
