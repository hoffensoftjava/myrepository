package com.example.tutorial.plugins;
import com.example.tutorial.plugins.exceptions.*;
public interface CronService {
	
	public void reschedule(long interval);
    
}