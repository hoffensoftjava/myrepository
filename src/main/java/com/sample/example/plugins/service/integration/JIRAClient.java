package com.example.tutorial.plugins;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.bc.issue.link.RemoteIssueLinkService;
import com.atlassian.jira.issue.link.RemoteIssueLinkBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.issue.link.RemoteIssueLink;
import com.atlassian.jira.security.JiraAuthenticationContext;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory; 
import com.atlassian.sal.api.pluginsettings.PluginSettings;

import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.component.ComponentAccessor;
import java.io.UnsupportedEncodingException;
import com.atlassian.jira.exception.CreateException;
import java.net.URLEncoder;
import javax.inject.Inject;


import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.util.*;

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;

import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.example.tutorial.plugins.ao.service.AOMappingService;

import com.atlassian.sal.api.user.UserManager;
import com.example.tutorial.plugins.*;
import com.example.tutorial.plugins.exceptions.*;


@ExportAsService
@Component
public class JIRAClient
{

  private final String SYNC_FIELD_NAME = "SNSYNC";
	@ComponentImport
	private final RequestFactory requestFactory; 
	@ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	private static final Logger log = LoggerFactory.getLogger(JIRAClient.class);
	@ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    @ComponentImport
    private final RemoteIssueLinkManager remoteIssueLinkManager;
    private FieldCollectionsUtils fieldCollectionsUtils;
    private IssueActionsUtils issueActionsUtils;
    private AOMappingService aoMappingService;
    private ApplicationUser currentAppUser;

    private  ServiceNowClient serviewNowClient;
    
  	public enum IssueAction {
        INSERT,UPDATE,DELETE
  	}

	@Inject
    public JIRAClient(RequestFactory requestFactory,PluginSettingsFactory pluginSettingsFactory,
    	JiraAuthenticationContext jiraAuthContext,FieldCollectionsUtils fieldCollectionsUtils,
        IssueActionsUtils issueActionsUtils,RemoteIssueLinkManager remoteIssueLinkManager,
        AOMappingService aoMappingService,ServiceNowClient serviewNowClient)
    {
         this.requestFactory = requestFactory;
         this.pluginSettingsFactory = pluginSettingsFactory;
         this.jiraAuthContext = jiraAuthContext;
         this.remoteIssueLinkManager = remoteIssueLinkManager;
         this.fieldCollectionsUtils = fieldCollectionsUtils;
         this.issueActionsUtils = issueActionsUtils;
         this.aoMappingService = aoMappingService;
         this.currentAppUser = this.jiraAuthContext.getUser();
         this.serviewNowClient = serviewNowClient;
         
    }

	public String pushSNService(Issue issueObject,IssueAction action,Set<String> fields) throws ResponseException,HSCustomFieldNotFoundException
    {
        Scheme scheme = aoMappingService.getSchemeForProject(issueObject.getProjectObject().getKey());
        ConnectionConfig connectionConfig = ConfigHelper.getConnectionConfig();
        String snBaseUrl = connectionConfig.getEndpointURL();
        String result = "";
        if(scheme != null)
        {
	        String jsonRequest = compileJSONInput(scheme,issueObject,fields);
	        log.info("Request JSON {}.",jsonRequest);
	        try
	        {
		        if(jsonRequest != null || jsonRequest != "{}")
		        {
		            if(action == IssueAction.INSERT){
		                result = serviewNowClient.saveIssueIntoServiceNow(jsonRequest,scheme.getSNObjectName());
		                JSONObject jsonResult = new JSONObject(result).getJSONObject("result");
		                String sysID = jsonResult.getString("sys_id");
                    String remoteURL = snBaseUrl.substring(snBaseUrl.length() - 1) != "/" ? snBaseUrl +"/" + ConfigHelper.SN_REMOTE_URL : snBaseUrl + ConfigHelper.SN_REMOTE_URL;
		                String sysUrl = String.format("%s?uri=%s.do?sys_id=%s",remoteURL,scheme.getSNObjectName(),sysID);
		                log.info("Endpoint URL of Servicenow issue : {}.", sysUrl);
		                IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
                    if(fieldCollectionsUtils.isIssueHasField(issueObject,fieldCollectionsUtils.getFieldFromKey("SNID")))
                    {
		                 issueActionsUtils.setFieldValue(currentAppUser,(MutableIssue)issueObject,fieldCollectionsUtils.getFieldIdByName("SNID"),sysID,changeHolder);
                    }
                    else
                    {
                      log.error("Custom Field(SNID) is not available when creating the Issue from JIRA to Servicenow");
                      throw new HSCustomFieldNotFoundException("Custom Field(SNID) is not available when creating the Issue from JIRA to Servicenow","SNID");
                    }
		                updateRemoteLink(sysUrl,issueObject);
                    aoMappingService.addSyncTransaction(issueObject.getId(),sysID,issueObject.getProjectObject().getKey());
		                aoMappingService.addSyncTransactionLog(jsonRequest,"PUSH",IssueAction.INSERT.toString(),new Date());
                }
                else if(action == IssueAction.UPDATE)
                {
                    String snId = "";
                    if(fieldCollectionsUtils.isIssueHasField(issueObject,fieldCollectionsUtils.getFieldFromKey("SNID")))
                    {
		                  snId = issueActionsUtils.getFieldStringValue(issueObject,fieldCollectionsUtils.getFieldIdByName("SNID"));
                    }
                    else
                    {
                      log.error("Custom Field(SNID) is not available when Updating the Issue from JIRA to Servicenow");
                      throw new HSCustomFieldNotFoundException("Custom Field(SNID) is not available when Updating the Issue from JIRA to Servicenow","SNID");
                    }
		                if(snId != null)
		                {
		                    result = serviewNowClient.updateIssueIntoServiceNow(jsonRequest,scheme.getSNObjectName(),snId);
                        aoMappingService.addSyncTransactionLog(jsonRequest,"PUSH",IssueAction.UPDATE.toString(),new Date());
		                }
		                else
	    				      {
	    					        log.info("Issue ({]}) has no Servicenow Identifier link.",issueObject.getKey());
	    				      }
		            }
                
		          }
	         }
	         catch(Exception e)
	         {
	              throw new RuntimeException(e);
	         }
	       }
	       else
	       {
	    	  log.info("Project({}) of the selected issue has no Scheme Mapping defined.",issueObject.getProjectObject().getName());
	       }
        return result;
    }

    private String compileJSONInput(Scheme scheme,Issue issueObject,Set<String> fields)
    {
          StringBuilder output = new StringBuilder();
          output.append("{");
          try
          {
          if(scheme == null)
          {
              return null;
          }
          else
          {
           

            for(FieldMapping fieldMap : scheme.getMappings())
            {
              if(fieldMap.getSyncType() != FieldMapping.SyncType.PULL)
              {
                if(fields!=null)
                {
                  for(String fieldKey :fields)
                  {
                    if(fieldKey.equals(fieldMap.getJIRAFieldName()))
                    {
                      Field field = issueActionsUtils.getFieldFromKey(fieldKey);
                      if(issueActionsUtils.getFieldValueFromIssue(issueObject,field) != null)
                      {
                        output.append(String.format("\"%s\":\"%s\",",fieldMap.getSNFieldName(),issueActionsUtils.getFieldValueFromIssue(issueObject,field)));
                      }
                    }
                  }
                }
                else
                {
                   List<Field> issueFields = fieldCollectionsUtils.getAvailableFields();
                  for(Field field :issueFields)
                  {
                    if(field.getId().equals(fieldMap.getJIRAFieldName()))
                    {
                      //Field field = issueActionsUtils.getFieldFromKey(fieldKey);
                      if(issueActionsUtils.getFieldValueFromIssue(issueObject,field) != null)
                      {
                        output.append(String.format("\"%s\":\"%s\",",fieldMap.getSNFieldName(),issueActionsUtils.getFieldValueFromIssue(issueObject,field)));
                      }
                    }
                  }
                }
              }
            }
          }
          output.append("}");
          if(output.length()-(output.length()-output.lastIndexOf(",")) > 0 )
          {
            output.deleteCharAt(output.length()-(output.length()-output.lastIndexOf(",")));
          }
          	return output.toString();
          }
          catch(Exception e)
          {
            throw new RuntimeException(e);
          }
    }
  public String updateRemoteLink(String snlink,Issue issueObject) throws CreateException
	{	
		RemoteIssueLinkBuilder remoteIssueLinkBuilder = new RemoteIssueLinkBuilder();
		remoteIssueLinkBuilder.issueId(issueObject.getId());			
		remoteIssueLinkBuilder.relationship("SN Ticket");
		remoteIssueLinkBuilder.title("Related Servicenow Ticket");
		remoteIssueLinkBuilder.url(snlink);
		RemoteIssueLink link = remoteIssueLinkManager.createRemoteIssueLink(remoteIssueLinkBuilder.build(), jiraAuthContext.getUser());
		return "";
        
	}
		
}