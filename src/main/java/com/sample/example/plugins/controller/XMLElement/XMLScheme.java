package com.example.tutorial.plugins.admin.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;
import javax.xml.bind.annotation.*;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLScheme
{
  @XmlElement public int id;
  @XmlElement public String schemeName;
  @XmlElement public String jiraObjectName;
  @XmlElement public String snObjectName;
  @XmlElement public String snParentObjects;
  @XmlElement public List<XMLFieldMapping> mappings; 
  @XmlElement public List<XMLProjectMapping> projectMappings; 
  
}