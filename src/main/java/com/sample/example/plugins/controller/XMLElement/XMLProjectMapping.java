package com.example.tutorial.plugins.admin.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.*;
import java.util.List;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLProjectMapping
{
  	@XmlElement public int id;
    @XmlElement public String projectName;
    @XmlElement public XMLScheme scheme;
    @XmlElement public String duration;
    @XmlElement public int interval;
}