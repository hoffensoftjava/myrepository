package com.example.tutorial.plugins.admin.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLFieldMapping
{
  @XmlElement public int id;
  @XmlElement public XMLScheme scheme;
  @XmlElement public String jiraFieldName;
  @XmlElement public String snFieldName;
  @XmlElement public String syncType;
  @XmlElement public String snFieldType;
}