package com.example.tutorial.plugins.admin.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Comparator;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLIssueField
{
  @XmlElement private String name;
  @XmlElement private String label;
        
  public String getName()
  {
    return name;
  }
        
  public void setName(String name)
  {
    this.name = name;
  }
        
  public String getLabel()
  {
    return label;
  }
        
  public void setLabel(String label)
  {
    this.label = label;
  }

  
public static Comparator<XMLIssueField> TableNameComparator = new Comparator<XMLIssueField>() {

	public int compare(XMLIssueField table1, XMLIssueField table2) {
	   String TableName1 = table1.getName().toUpperCase();
	   String TableName2 = table2.getName().toUpperCase();
	   return TableName1.compareTo(TableName2);
    }};

 }