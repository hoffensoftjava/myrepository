package com.example.tutorial.plugins.admin.XmlElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Comparator;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLSNTable
{
  @XmlElement private String name;
  @XmlElement private String label;
  @XmlElement private String link;
  @XmlElement private String sysid;
  @XmlElement private String type;
        
  public String getName()
  {
    return name;
  }
        
  public void setName(String name)
  {
    this.name = name;
  }
        
  public String getLabel()
  {
    return label;
  }
        
  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getLink()
  {
    return link;
  }
        
  public void setLink(String link)
  {
    this.link = link;
  }

  public String getSysId()
  {
    return sysid;
  }
        
  public void setSysId(String sysid)
  {
    this.sysid = sysid;
  }
  public String getType()
  {
    return type;
  }
        
  public void setType(String type)
  {
    this.type = type;
  }

public static Comparator<XMLSNTable> TableLabelComparator = new Comparator<XMLSNTable>() {

	public int compare(XMLSNTable table1, XMLSNTable table2) {
	   String TableName1 = table1.getLabel().toUpperCase();
	   String TableName2 = table2.getLabel().toUpperCase();
	   return TableName1.compareTo(TableName2);
    }};

 }