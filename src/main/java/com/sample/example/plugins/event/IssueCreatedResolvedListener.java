package com.example.tutorial.plugins;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.jira.issue.comments.Comment;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.Enumeration;
import net.java.ao.Query;

import java.util.ArrayList; 
import java.util.List;
import java.util.*;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory; 
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;

import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.atlassian.sal.api.user.UserManager;
import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.util.*;
import com.example.tutorial.plugins.JIRAClient;

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.core.util.map.EasyMap;

/**
 * Simple JIRA listener using the atlassian-event library and demonstrating
 * plugin lifecycle integration.
 */
@ExportAsService
@Component
public class IssueCreatedResolvedListener {

    private static final Logger log = LoggerFactory.getLogger(IssueCreatedResolvedListener.class);
    private String baseURL = "https://dev10629.service-now.com/api/now/table/";
    private static final String SN_SYNTAX = "COMMENT FROM : SERVICENOW BY";

    private final RequestFactory requestFactory; 
    private AOMappingService aoMappingService;
    private ServiceNowClient serviewNowClient;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    private FieldCollectionsUtils fieldCollectionsUtils;
    private IssueActionsUtils issueActionsUtils;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private JIRAClient jiraClient;

    

    /**
     * Constructor.
     * @param eventPublisher injected {@code EventPublisher} implementation.
     */
     @Autowired
    public IssueCreatedResolvedListener(@ComponentImport final EventPublisher eventPublisher,
        @ComponentImport final RequestFactory requestFactory,AOMappingService aoMappingService,
        PluginSettingsFactory pluginSettingsFactory,FieldCollectionsUtils fieldCollectionsUtils,
        IssueActionsUtils issueActionsUtils,JiraAuthenticationContext jiraAuthContext,JIRAClient jiraClient,ServiceNowClient serviceNowClient)
    {
         eventPublisher.register(this);
         this.requestFactory = requestFactory;
         this.aoMappingService = aoMappingService;
         this.pluginSettingsFactory = pluginSettingsFactory;
         this.fieldCollectionsUtils = fieldCollectionsUtils;
         this.issueActionsUtils = issueActionsUtils;
         this.jiraAuthContext = jiraAuthContext;
         this.currentAppUser = this.jiraAuthContext.getUser();
         this.serviewNowClient =  serviceNowClient;
         this.jiraClient = jiraClient;
         
    }

    /**
     * Called when the plugin has been enabled.
     * @throws Exception
     */
    /*
    @Override
    public void afterPropertiesSet() throws Exception {
        // register ourselves with the EventPublisher
        eventPublisher.register(this);
    }
    */
    /**
     * Called when the plugin is being disabled or removed.
     * @throws Exception
     */
    /*
    @Override
    public void destroy() throws Exception {
        // unregister ourselves with the EventPublisher
        eventPublisher.unregister(this);
    }
    */
    /**
     * Receives any {@code IssueEvent}s sent by JIRA.
     * @param issueEvent the IssueEvent passed to us
     */
    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
         Long eventTypeId = issueEvent.getEventTypeId();
        Issue issue = issueEvent.getIssue();
        Comment comment = issueEvent.getComment();
        String eventBlurb = null;
        log.debug("Value of EventTypeID {}",eventTypeId);
        log.debug("Value of Event {}",issueEvent);
    if (eventTypeId.equals(EventType.ISSUE_CREATED_ID)) {
            try
            {

                String response = jiraClient.pushSNService(issue,JIRAClient.IssueAction.INSERT,null);
            }
            catch(ResponseException e) {
                log.error("unexpected exception while trying to Create JIRA Issue", e);
            }
        }

        else if(eventTypeId.equals(EventType.ISSUE_UPDATED_ID)){
            try
            {
              List<GenericValue> changeItems = null;
              GenericValue changeLog  = null;
              try {
                  changeLog = issueEvent.getChangeLog();
                  if(changeLog!=null)
                  {
                    changeItems = changeLog.internalDelegator.findByAnd("ChangeItem", EasyMap.build("group", changeLog.get("id")));
                  }
                } 
              catch (GenericEntityException e){
                    log.error("unexpected exception while trying to update JIRA Issue", e);
                  } 

              if (changeItems != null) {
                    Set<String> fields = new HashSet<String>();
                    for (GenericValue genericValue : changeItems) {
                     fields.add(genericValue.getString("field"));
                    }
                    if(!fields.contains(fieldCollectionsUtils.getFieldIdByName("SNSYNC")))
                    {
                        String response = jiraClient.pushSNService(issue,JIRAClient.IssueAction.UPDATE,fields);      
                    }
                    else
                    {
                      log.info("THIS EVENT IS REDUNDANT EVENT");
                    }
                }
            }
                catch(ResponseException e) {
                log.error("unexpected exception while trying to update JIRA Issue", e);
            }
        }

        else if(eventTypeId.equals(EventType.ISSUE_COMMENTED_ID))
        {
            try
            {
                String response = jiraClient.pushSNService(issue,JIRAClient.IssueAction.UPDATE,null);
            }
                catch(ResponseException e) {
                log.error("unexpected exception while trying to ISSUE_COMMENT_EDITED_ID JIRA Issue", e);
            }

        }
        //sys_audit_list.do?sysparm_query=documentkey=7a98abcb0f31220092f7c09ce1050eaf
        //sys_history_set_list.do?sysparm_query=id=7a98abcb0f31220092f7c09ce1050eaf
       else if(eventTypeId.equals(EventType.ISSUE_COMMENT_EDITED_ID))
        {
            try
            {
                String response = jiraClient.pushSNService(issue,JIRAClient.IssueAction.UPDATE,null);
            }
                catch(ResponseException e) {
                log.error("unexpected exception while trying to ISSUE_COMMENT_EDITED_ID JIRA Issue", e);
            }

        }
        else if(eventTypeId.equals(EventType.ISSUE_WORKLOG_UPDATED_ID) || eventTypeId.equals(EventType.ISSUE_WORKLOGGED_ID)){
            try
            {
                String response = jiraClient.pushSNService(issue,JIRAClient.IssueAction.UPDATE,null);
            }
                catch(ResponseException e) {
                log.error("unexpected exception while trying to ISSUE_WORKLOG_UPDATED_ID/ISSUE_WORKLOGGED_ID JIRA Issue", e);
            }

        }        
    }
}