package com.sample.example.plugins.api;

public interface MyPluginComponent
{
    String getName();
}