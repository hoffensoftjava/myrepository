package com.example.tutorial.plugins.admin.menu.conditions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.web.Condition;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.example.tutorial.plugins.*;

public class IsSNConfiguredCondition implements Condition {
 
    //@ComponentImport
    //private final ActiveObjects ao;
    private static final Logger log = LoggerFactory.getLogger(IsSNConfiguredCondition.class);
    public IsSNConfiguredCondition()
    {
        //this.ao=ao;
    }
    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        // nothing to do here, no params are needed
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        ConnectionConfig connectionConfig = ConfigHelper.getConnectionConfig();
         boolean hasEntry =  (connectionConfig != null);
         return hasEntry;
        
        
    }
}