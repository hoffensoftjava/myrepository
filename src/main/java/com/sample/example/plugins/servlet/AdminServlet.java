
package com.example.tutorial.plugins;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import com.atlassian.sal.api.user.UserManager;

import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;
import com.example.tutorial.plugins.CronService;
import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.exceptions.*;

@Scanned
public class AdminServlet extends javax.servlet.http.HttpServlet {

	private static final String PLUGIN_STORAGE_KEY = "com.example.tutorial.plugins.auth";
    private static final Logger log;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    private AOMappingService aoMappingService; 
    private CronService cronService;
    private ServiceNowClient serviceNowClient;
    private static final String ADMIN_TEMPLATE = "/templates/admin/admin.vm";
    private static final String SAVE = "Save Settings";
    private static final String CHECK = "Test Connection";

    static {
        log = LoggerFactory.getLogger(AdminServlet.class);
    }

    @Inject
    public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, 
        TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory,
        AOMappingService aoMappingService,CronService cronService,ServiceNowClient serviceNowClient)
    {
        super();
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.aoMappingService = aoMappingService;
        this.cronService = cronService;
        this.serviceNowClient = serviceNowClient;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        UserKey userkey = userManager.getRemoteUserKey(request);
        if (userkey == null || !userManager.isSystemAdmin(userkey)) {
            redirectToLogin(request, response);
            return;
        }
        Map<String, Object> context = Maps.newHashMap();

        // PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();

        // if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".endpoint") == null){
        //     String endpoint = "Enter a Service now Endpoint URL here.";
        //     pluginSettings.put(PLUGIN_STORAGE_KEY +".endpoint", endpoint);
        // }

        // if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".username") == null){
        //     String username = "Enter your username here.";
        //     pluginSettings.put(PLUGIN_STORAGE_KEY + ".username", username);
        // }

        // if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".password") == null){
        //     String password = "Enter your password here.";
        //     pluginSettings.put(PLUGIN_STORAGE_KEY + ".password", password);
        // }

        // if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".auth") == null){
        //     String auth = "Please select the Authentication type.";
        //     pluginSettings.put(PLUGIN_STORAGE_KEY + ".auth", auth);
        // }
        // if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".isServiceEnabled") == null){
        //     // String auth = "false";
        //     // pluginSettings.put(PLUGIN_STORAGE_KEY + ".isServiceEnabled", auth);
        // }

        ConnectionConfig conectionConfi = aoMappingService.getConnectionConfig();

        if(conectionConfi!=null)
        {
            context.put("endpoint", conectionConfi.getEndpointURL());
            context.put("username", conectionConfi.getUserName());
            context.put("password", conectionConfi.getPassword());
            context.put("auth", conectionConfi.getAuthType());
            context.put("interval", conectionConfi.getInterval());
            context.put("duration", conectionConfi.getDuration());
        }
        else
        {
            context.put("endpoint", "");
            context.put("username", "");
            context.put("password", "");
            context.put("auth", "BASIC");
            context.put("interval", "");

        }
        context.put("isFirstTabActive", "true");
        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render(ADMIN_TEMPLATE,context,response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException
    {
        String action = req.getParameter("action");
        log.debug("action: {}",req.getParameter("action"));
        String jsonErrorMessage = "";
        Map<String, Object> context = Maps.newHashMap();
        if(SAVE.equals(action))
        {
            ConnectionConfig connectionConfig = aoMappingService.submitConnectionConfig(req.getParameter("endpoint"),req.getParameter("username"),
                req.getParameter("password"),ConnectionConfig.AuthType.valueOf(req.getParameter("authtype")),Integer.parseInt(req.getParameter("intervel_time")),
                ConnectionConfig.Duration.valueOf(req.getParameter("intervel_duration")));
            try
            {
                long interval = getIntervalMillisec(connectionConfig);
                log.debug("interval: {}",interval);
                cronService.reschedule(interval);
            }
            catch(Exception e)
            {
                if(e instanceof HSJSONNotFormattedException)
                {
                    log.error("HSJSONNotFormattedException Log is {}",e.getMessage());
                    jsonErrorMessage = ((HSJSONNotFormattedException)e).getJSONString();    
                }
                else if (e instanceof HSResponseException)
                {
                    log.error("HSResponseException Log is {}",((HSResponseException)e).getErrorString());
                    jsonErrorMessage = ((HSResponseException)e).getErrorString();       
                }
                
            }
            context.put("isFirstTabActive", "true");
            //response.sendRedirect(req.getHeader("Referer"));   
        } 
        else if(CHECK.equals(action))
        {
            try
            {
                serviceNowClient.checkValidConnection();    
            }
            catch(HSResponseException e)
            {
                log.error("HSResponseException Log is {}",((HSResponseException)e).getErrorString());
                jsonErrorMessage = ((HSResponseException)e).getErrorString(); 
            }
            catch(HSJSONNotFormattedException e)
            {
                log.error("HSJSONNotFormattedException Log is {}",e.getMessage());
                jsonErrorMessage = ((HSJSONNotFormattedException)e).getJSONString();   
            }
            context.put("isFirstTabActive", "false");
        }

            ConnectionConfig conectionConfi = aoMappingService.getConnectionConfig();
            if(conectionConfi!=null)
            {
                context.put("endpoint", conectionConfi.getEndpointURL());
                context.put("username", conectionConfi.getUserName());
                context.put("password", conectionConfi.getPassword());
                context.put("auth", conectionConfi.getAuthType());
                context.put("interval", conectionConfi.getInterval());
                context.put("duration", conectionConfi.getDuration());
            }
            if(jsonErrorMessage != "")
            {
                if(context.get("isFirstTabActive").equals("true"))
                {
                      context.put("configErrors",jsonErrorMessage);  
                }
                else
                {
                    context.put("connectionErrors",jsonErrorMessage);
                }
            } 
            else
            {
                if(context.get("isFirstTabActive").equals("false"))
                {
                      context.put("connectionSuccess",jsonErrorMessage);  
                }
                else
                {
                    context.put("configSuccess",jsonErrorMessage);
                }
            }
            response.setContentType("text/html;charset=utf-8");
            templateRenderer.render(ADMIN_TEMPLATE, context, response.getWriter());
       
    }

    private long getIntervalMillisec(ConnectionConfig connectionConfig) {

            long intervalSeconds = 5*60*1000;
            if(connectionConfig!=null)
            {
                if(connectionConfig.getDuration().toString() == "MINUTES")
                {
                    intervalSeconds = (long)(connectionConfig.getInterval() * 60 * 1000);
                }
                else if(connectionConfig.getDuration().toString() == "HOURS")
                {
                    intervalSeconds =  (long)(connectionConfig.getInterval() * 60 * 60 * 1000);
                }
                else if(connectionConfig.getDuration().toString() == "DAYS")
                {
                    intervalSeconds =  (long)(connectionConfig.getInterval() * 24 * 60 * 60 * 1000);
                }
            }
            return intervalSeconds;
        }
    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
