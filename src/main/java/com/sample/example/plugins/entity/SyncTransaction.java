package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;

@Preload
public interface SyncTransaction extends Entity {

  public Long getJIRAId();
  public void setJIRAId(Long jira_id);

  public String getSNId();
  public void setSNId(String sn_id);

  public String getProjectId();
  public void setProjectId(String projectId);

}