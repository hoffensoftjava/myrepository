package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import java.util.*;

@Preload
public interface ConnectionConfig extends Entity {

  public enum AuthType {
        BASIC,
        OAUTH
  }
  public enum Duration {
        MINUTES, HOURS, DAYS,WEEKS
  }
  public String getEndpointURL();
  public void setEndpointURL(String endPointUrl);

  public String getUserName();
  public void setUserName(String userName);

  public String getPassword();
  public void setPassword(String password);

  public AuthType getAuthType();
  public void setAuthType(AuthType authType);

  public int getInterval();
  public void setInterval(int interval);

  public Duration getDuration();
  public void setDuration(Duration duration);

}