package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;

@Preload
public interface ProjectMapping extends Entity {


  public enum Duration {
        MINUTES, HOURS, DAYS,WEEKS,MONTHS
  }

  public Scheme getScheme();
  public void setScheme(Scheme scheme);

  public String getProjectName();
  public void setProjectName(String projectname);

  public int getInterval();
  public void setInterval(int interval);

  public Duration getDuration();
  public void setDuration(Duration duration);
}