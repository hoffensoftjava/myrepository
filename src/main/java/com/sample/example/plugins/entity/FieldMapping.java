package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;

@Preload
public interface FieldMapping extends Entity {

public enum SyncType {
        PUSH, PULL, BOTH
    }

  public String getJIRAFieldName();
  public void setJIRAFieldName(String jira_field_name);

  public String getSNFieldName();
  public void setSNFieldName(String sn_field_name);

  public String getSNFieldType();
  public void setSNFieldType(String sn_field_type);

  public SyncType getSyncType();
  public void setSyncType(SyncType sync_type);

  public Scheme getScheme();
  public void setScheme(Scheme scheme);

}