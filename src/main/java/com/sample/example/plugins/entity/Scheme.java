package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;

@Preload
public interface Scheme extends Entity {

  public String getSchemeName();
  public void setSchemeName(String scheme_name);

  public String getJIRAObjectName();
  public void setJIRAObjectName(String jira_object_name);

  public String getSNObjectName();
  public void setSNObjectName(String sn_object_name);

  //Comma seperated values
  public String getSNParentObjects();
  public void setSNParentObjects(String sn_parent_name);

  @OneToMany
  public FieldMapping[] getMappings();

  @OneToMany
  public ProjectMapping[] getProjectMappings();
}