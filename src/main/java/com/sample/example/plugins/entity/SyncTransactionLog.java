package com.example.tutorial.plugins.admin.ao;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import java.util.*;

@Preload
public interface SyncTransactionLog extends Entity {

  public String getContent();
  public void setContent(String content);

  public String getSyncType();
  public void setSyncType(String type);

  public String getAction();
  public void setAction(String action);

  public Date getUpdatedDate();
  public void setUpdatedDate(Date updatedDate);

}