
package com.example.tutorial.plugins;

import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.lifecycle.LifecycleManager;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.GuardedBy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.EnumSet;
import java.util.Set;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.MutableIssue;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.ServiceNowClient;

@ExportAsService
@Component
@Named("CronServiceImpl")
public class CustomFieldInstaller implements InitializingBean, DisposableBean {

	 /* package */ //static final String KEY = CronServiceImpl.class.getName() + ":instance";
	    //private static final String JOB_NAME = CronServiceImpl.class.getName() + ":job";

		private static final Logger log = LoggerFactory.getLogger(CustomFieldInstaller.class);
	    private final PluginScheduler pluginScheduler;  // provided by SAL
	    private AOMappingService aoMappingService;
	    private ServiceNowClient serviewNowClient;

	    private static final String SNID = "SNID";
	    private static final String SNSYNC = "SNSYNC";
	    
	    
     	private final CustomFieldManager customFieldManager;
    	private final FieldScreenManager fieldScreenManager;

	    @Inject
	    public CustomFieldInstaller(@ComponentImport final PluginScheduler pluginScheduler,AOMappingService aoMappingService,
	    	ServiceNowClient serviewNowClient,@ComponentImport final CustomFieldManager customFieldManager,
	    	@ComponentImport final FieldScreenManager fieldScreenManager) {
	        this.pluginScheduler = pluginScheduler;
	        this.aoMappingService = aoMappingService;
	        this.serviewNowClient = serviewNowClient;
	        this.customFieldManager = customFieldManager;
	        this.fieldScreenManager = fieldScreenManager;
	    }

		@Override
	    public void destroy() throws Exception {
	        //Get the already installed custom field by name
	        
	        // CustomField snidField = this.customFieldManager.getCustomFieldObjectByName(SNID);
	        // CustomField snsyncField = this.customFieldManager.getCustomFieldObjectByName(SNSYNC);
	        // //Remove if not null
	        // if (snidField != null) {
	        //     this.customFieldManager.removeCustomField(snidField);
	        // }
	        // if (snsyncField != null) {
	        //     this.customFieldManager.removeCustomField(snsyncField);
	        // }
	    }
	    @Override
    	public void afterPropertiesSet() throws Exception {
	        //Create a list of issue types for which the custom field needs to be available
	        /*
	        List<IssueType> issueTypes = new ArrayList<IssueType>();
	        issueTypes.add(null);

	        //Create a list of project contexts for which the custom field needs to be available
	        List<JiraContextNode> contexts = new ArrayList<JiraContextNode>();
	        contexts.add(GlobalIssueContext.getInstance());
	       

	        FieldScreen defaultScreen = fieldScreenManager.getFieldScreen(FieldScreen.DEFAULT_SCREEN_ID);
	        if (customFieldManager.getCustomFieldObjectByName(SNID) == null) {
		        CustomField snidField = this.customFieldManager.createCustomField(SNID, "Servicenow Identifier",
		            this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
		            this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
		            contexts, issueTypes);
		        
		        if (!defaultScreen.containsField(snidField.getId())) {
		            FieldScreenTab firstTab = defaultScreen.getTab(0);
		            firstTab.addFieldScreenLayoutItem(snidField.getId());
		        }
	     	}
	     	if (customFieldManager.getCustomFieldObjectByName(SNSYNC) == null) {
		        CustomField snsyncField = this.customFieldManager.createCustomField(SNSYNC, "Servicenow Sync Identifier",
		            this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
		            this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
		            contexts, issueTypes);
		        
		        if (!defaultScreen.containsField(snsyncField.getId())) {
		            FieldScreenTab firstTab = defaultScreen.getTab(0);
		            firstTab.addFieldScreenLayoutItem(snsyncField.getId());
		        }
	     	}
	     	*/

    	}

}