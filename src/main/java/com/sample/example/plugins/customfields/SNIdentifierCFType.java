package com.example.tutorial.plugins.admin.customfields;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.inject.Inject;


@Scanned
public class SNIdentifierCFType extends GenericTextCFType
{
	@ComponentImport 
	private final CustomFieldValuePersister customFieldValuePersister;
    @ComponentImport 
    private final GenericConfigManager genericConfigManager;
	
    @Inject
	public SNIdentifierCFType(CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager)
	{
		super(customFieldValuePersister, genericConfigManager);
		this.customFieldValuePersister = customFieldValuePersister;
        this.genericConfigManager = genericConfigManager;

	}
}