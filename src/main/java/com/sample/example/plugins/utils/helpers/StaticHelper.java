
package com.example.tutorial.plugins;

import org.springframework.stereotype.Component;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser; 

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.example.tutorial.plugins.ServiceNowClient;
import com.example.tutorial.plugins.admin.util.*;

@ExportAsService
@Component
@Named("StaticHelper")
public class StaticHelper {
   
  private static IssueManager issueManager;
	private static RequestFactory requestFactory;
	private static CommentManager commentManager;
	private static JiraAuthenticationContext jiraAuthContext;
  private static PluginSettingsFactory pluginSettingsFactory;
  private static ServiceNowClient serviceNowClient;
  private static ServiceNowFieldUtils serviceNowFieldUtils;
  private static FieldCollectionsUtils fieldCollectionsUtils;
  private static IssueActionsUtils issueActionsUtils;
 	
	@Inject
  	public StaticHelper(@ComponentImport IssueManager issueManager,@ComponentImport  RequestFactory requestFactory,
  		@ComponentImport  CommentManager commentManager,@ComponentImport  JiraAuthenticationContext jiraAuthContext,
      @ComponentImport PluginSettingsFactory pluginSettingsFactory,ServiceNowClient serviceNowClient,
      ServiceNowFieldUtils serviceNowFieldUtils,FieldCollectionsUtils fieldCollectionsUtils,IssueActionsUtils issueActionsUtils) {

    	StaticHelper.issueManager = issueManager;
    	StaticHelper.requestFactory = requestFactory;
    	StaticHelper.commentManager = commentManager;
    	StaticHelper.jiraAuthContext = jiraAuthContext;
      StaticHelper.pluginSettingsFactory = pluginSettingsFactory;
      StaticHelper.serviceNowClient = serviceNowClient;
      StaticHelper.serviceNowFieldUtils = serviceNowFieldUtils;
      StaticHelper.issueActionsUtils = issueActionsUtils;
      StaticHelper.fieldCollectionsUtils = fieldCollectionsUtils;
  	}
 
  	public static IssueManager getIssueManager() {
    	return issueManager;
  	}
  	public static RequestFactory getRequestFactory()
  	{
  		return requestFactory;
  	}
  	public static CommentManager getCommentManager()
  	{
  		return commentManager;
  	}
  	public static JiraAuthenticationContext getJiraAuthContext()
  	{
  		return jiraAuthContext;
  	}
    public static PluginSettingsFactory getPluginSettingsFactory()
    {
      return pluginSettingsFactory;
    }
    public static ServiceNowClient getServiceNowClient()
    {
      return serviceNowClient;
    }
    public static ServiceNowFieldUtils getServiceNowFieldUtils()
    {
      return serviceNowFieldUtils;
    }
    public static FieldCollectionsUtils getFieldCollectionsUtils()
    {
      return fieldCollectionsUtils;
    }
    public static IssueActionsUtils getIssueActionsUtils()
    {
      return issueActionsUtils;
    }
}