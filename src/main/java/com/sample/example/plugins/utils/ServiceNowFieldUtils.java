package com.example.tutorial.plugins.admin.util;

import java.util.*;

import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.RequestFactory; 

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

@ExportAsService
@Component
public class ServiceNowFieldUtils
{
	private static final Logger log = LoggerFactory.getLogger(ServiceNowFieldUtils.class);

	private final Map<String, String> specialSNFieldWithAPI;

	@ComponentImport
	private final RequestFactory requestFactory; 

	@Inject
	public ServiceNowFieldUtils(final RequestFactory requestFactory)
	{
		this.requestFactory = requestFactory;
		specialSNFieldWithAPI= new HashMap<String,String>();
	}
	public Map<String, String> getSpecialSNFieldWithAPI() {
		specialSNFieldWithAPI.put("JOURNAL_INPUT","sys_journal_field?sysparm_query=element_id=%s%selement=%s%ssys_created_on%s('%s','%s')");
		specialSNFieldWithAPI.put("JOURNAL","sys_journal_field?sysparm_query=element_id=%s%selement=%s%ssys_created_on%s('%s','%s')");
        return specialSNFieldWithAPI;
    }
    private Object getValueFromJson(JSONObject jObject,String searchKey,Object obj) throws JSONException 
        {
        	Iterator<?> keys = jObject.keys();
				while( keys.hasNext()) {
					String key = (String)keys.next();
					if (key.equals(searchKey)) {
						obj = jObject.get(key);
					}
					else if (jObject.get(key) instanceof JSONObject ) {
						obj = getValueFromJson((JSONObject)jObject.get(key), searchKey,obj);
					}
					else if (jObject.get(key) instanceof JSONArray ) {
						JSONArray jar = (JSONArray)jObject.get(key);
						for (int i = 0; i < jar.length(); i++) {
							JSONObject j = jar.getJSONObject(i);
							getValueFromJson(j, searchKey,obj);
						}
					}
    			}
    			return obj;
        }
}