package com.example.tutorial.plugins;

import org.springframework.stereotype.Component;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import javax.inject.Inject;
import javax.inject.Named;
import com.atlassian.jira.issue.IssueManager;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.example.tutorial.plugins.admin.ao.ConnectionConfig;

@ExportAsService
@Component
@Named("ConfigHelper")
public class ConfigHelper {
	private static IssueManager issueManager;
	public static String CONTENT_TYPE = "application/json";
	public static String TABLE_API = "api/now/table/";
	public static String COLUMN_TABLE_NAME = "sys_dictionary";
	public static String TABLE_NAME = "sys_db_object";
	public static String SN_COMMENT_SUFFIX = "%s. Source: SERVICENOW By : %s";
	public static String JIRA_COMMENT_SUFFIX= ". Source: JIRA By ";
	public static String SYNC_FIELD_NAME = "SNSYNC";
	public static String SN_IDENTIFIER_NAME = "SNID";
	public static String SN_REMOTE_URL = "nav_to.do";
	private static AOMappingService aOMappingService;
	
	@Inject
	public ConfigHelper(@ComponentImport IssueManager issueManager,AOMappingService aOMappingService)
	{
		ConfigHelper.issueManager = issueManager;
		ConfigHelper.aOMappingService = aOMappingService;
	}
	public static AOMappingService getAOMappingService()
	{
		return aOMappingService;
	}
	public static ConnectionConfig getConnectionConfig()
	{
		return aOMappingService.getConnectionConfig();	
	}


}