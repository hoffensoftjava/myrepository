package com.example.tutorial.plugins;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.MutableIssue;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;


import com.example.tutorial.plugins.ServiceNowClient;

@Named("CreateChangeRequestActionSupport")
public class CreateChangeRequestActionSupport extends JiraWebActionSupport
{
	private static final String PLUGIN_STORAGE_KEY = "com.example.tutorial.plugins.auth";
	private static final Logger log = LoggerFactory.getLogger(CreateChangeRequestActionSupport.class);
	private final IssueManager issueManager;
	private Issue currentIssue;
	private final RequestFactory requestFactory; 
	private Long id;
	private ServiceNowClient serviewNowClient;
	private final CommentManager commentManager;
	private JiraAuthenticationContext jiraAuthContext;
	private ApplicationUser currentAppUser;
	@ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

	@Inject
	public CreateChangeRequestActionSupport(@ComponentImport final RequestFactory requestFactory,@ComponentImport final IssueManager issueManager,
		@ComponentImport CommentManager commentManager,@ComponentImport JiraAuthenticationContext jiraAuthContext,
    PluginSettingsFactory pluginSettingsFactory,ServiceNowClient serviewNowClient)
	{
		log.info("Entering Constructor");
		this.issueManager = issueManager;
		this.requestFactory = requestFactory;
		this.commentManager = commentManager;
		this.jiraAuthContext = jiraAuthContext;
		this.currentAppUser = this.jiraAuthContext.getUser();
		this.serviewNowClient = serviewNowClient;
		this.pluginSettingsFactory = pluginSettingsFactory;
		log.info("Completed Constructor");
	}

	@Override
	protected void doValidation()
	{
		log.info("Entering doValidation");
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
      	String n = (String) e.nextElement();
      	String[] vals = request.getParameterValues(n);
      	log.debug("name " + n + ": " + vals[0]);
      	
      	if (currentIssue == null) {
      		log.error("The local variable didn't get set");
      		return;
    		}
   		 }
	}

	@Override
	protected String doExecute() throws Exception
	{
		log.info("Entering into doExecute() Method");
		try
            {
            	PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            	log.info("Endpoint Value received from GLOBAL SETTINGS : {}",pluginSettings.get(PLUGIN_STORAGE_KEY + ".endpoint"));
            	if (pluginSettings.get(PLUGIN_STORAGE_KEY + ".endpoint") != null){
         		   	
         		   	log.info("Endpoint Value received from GLOBAL SETTINGS : {}",pluginSettings.get(PLUGIN_STORAGE_KEY + ".endpoint"));
        		}

            	currentIssue = issueManager.getIssueObject(id);
                String response = serviewNowClient.createChangeRequest(currentIssue);
                if(response != null)
                {
                	MutableIssue mutableissue = issueManager.getIssueObject(id);
                   	String body = String.format("Change Request Created in Service now. %s",currentIssue.getKey());
                	commentManager.create(mutableissue,this.currentAppUser,body,true);
                }
                log.info("Change Request {} has been created for the issue Number {}.", response, currentIssue.getKey());

            }
                catch(ResponseException e) {
                log.error("unexpected exception while trying to Create Change Request in ServiceNow application", e);
                return ERROR;
            }
            return returnComplete("/browse/" + currentIssue.getKey());
	}

	@Override
	public String doDefault() throws Exception {
    	
    	return INPUT;
  	}

  	public Long getId() {
      return id;
  	}

  	public void setId(Long id) {
      this.id = id;
  	}











}