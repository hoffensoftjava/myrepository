
package com.example.tutorial.plugins.admin.config;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.collect.Maps;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.Enumeration;
import net.java.ao.Query;

import java.util.ArrayList; 
import java.util.List;
import java.util.*;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 

import com.atlassian.sal.api.user.UserManager;
import com.example.tutorial.plugins.ServiceNowClient;

import com.example.tutorial.plugins.admin.ao.Scheme;
import com.example.tutorial.plugins.admin.ao.FieldMapping;
import com.example.tutorial.plugins.admin.ao.ProjectMapping;
import com.example.tutorial.plugins.ao.service.AOMappingService;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import java.util.Set;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.example.tutorial.plugins.admin.controller.api.Table;
import com.example.tutorial.plugins.admin.controller.impl.SNTables;
import com.example.tutorial.plugins.admin.controller.api.Column;
import com.example.tutorial.plugins.admin.controller.impl.SNColumns;
import com.example.tutorial.plugins.admin.controller.impl.NameValueFactory;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
import com.example.tutorial.plugins.admin.util.FieldCollectionsUtils;
import com.example.tutorial.plugins.admin.ao.SyncTransactionLog;
import com.example.tutorial.plugins.exceptions.*;

@Named("getMappingConfigs")
public class getMappingConfigs extends JiraWebActionSupport {

	private static final String PLUGIN_STORAGE_KEY = "com.example.tutorial.plugins.admin.config";
    private static final Logger log;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final ProjectManager projectManager;

    private ServiceNowClient serviewNowClient;
    private AOMappingService aoMappingService;
    private Scheme scheme;
    private List<Field> fields = new ArrayList<Field>();
    private List<Table> snTables = new ArrayList<Table>();
    private final FieldCollectionsUtils fieldCollectionsUtils;

    private String schemename;
    private String jiraobjectname;
    private String snobjectname;
    private String jsonErrorMessage = "";

    
    static {
        log = LoggerFactory.getLogger(getMappingConfigs.class);
    }
 
    @Inject
    public getMappingConfigs(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,AOMappingService aoMappingService,ProjectManager projectManager,
        FieldCollectionsUtils fieldCollectionsUtils,ServiceNowClient serviewNowClient)
    {
        super();
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.serviewNowClient = serviewNowClient;
        this.aoMappingService = aoMappingService ;
        this.projectManager = projectManager;
        this.fieldCollectionsUtils = fieldCollectionsUtils;
         
    }

    @Override
    protected void doValidation()
    {
        log.info("Entering doValidation");
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
        String n = (String) e.nextElement();
        String[] vals = request.getParameterValues(n);
        log.debug("name " + n + ": " + vals[0]);
        
        // if (currentIssue == null) {
        //     log.error("The local variable didn't get set");
        //     return;
        //     }
        }
    }

    @Override
    protected String doExecute() throws Exception
    {
        log.info("Entering into doExecute(from getMappingScheme) Class");
        return returnComplete("/jira/secure/Dashboard.jspa"); //returnComplete("/browse/" + currentIssue.getKey());
    }

    @Override
    public String doDefault() throws Exception {
        loadAllIssueFields(); 
        try
        {
            loadAllSNTableNames(); 
        }
        catch(Exception  e)
        {

            if(e instanceof HSJSONNotFormattedException)
            {
                log.error("HSJSONNotFormattedException Log is {}",e.getMessage());
                jsonErrorMessage = ((HSJSONNotFormattedException)e).getJSONString();    
            }
            else if (e instanceof HSResponseException)
            {
                log.error("HSResponseException Log is {}",((HSResponseException)e).getErrorString());
                jsonErrorMessage = ((HSResponseException)e).getErrorString();       
            }
            
            return ERROR;
        }
        getAllSchemes();
        return INPUT;
    }
    public String getErrorMessage()
    {
        return jsonErrorMessage;
    }
    public String doUpdateSchemeDefault() throws Exception{
        loadAllSNTableNames();
        loadAllIssueFields(); 
        return INPUT;
    }
    public String doProjectMappingDefault() throws Exception {
        return INPUT;
    }
    public String doProjectListDefault() throws Exception {
        return INPUT;
    }
    public String doSchemeListDefault() throws Exception {
        return INPUT;
    }
    public String doLogListDefault() throws Exception {
        return INPUT;
    }
    public Scheme getScheme()
    {
       if(getSchemeId() != null)
       {
        Scheme scheme = aoMappingService.getScheme(Integer.parseInt(getSchemeId()));
        if(scheme != null)
        {
            return scheme;
        } 
       }
       return null;
    }

    private void loadAllIssueFields()
    {
        try
        {
            List<Field> issueFields =fieldCollectionsUtils.getAvailableFields();
            log.info("All Issue Fields");
            for(Field issueField : issueFields)
            {
                fields.add(issueField);
            }
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    private void loadAllSNTableNames() throws HSJSONNotFormattedException,HSResponseException
    {
        String jsonRequest = "";
        try
        {
           jsonRequest = serviewNowClient.fetchAllTables(); 
        }
        catch(HSResponseException e)
        {
            log.error(e.getErrorString());
            throw new HSResponseException(e.getErrorString(),e.getErrorString());
        }
        try
        {
           
            JSONArray jsonResult = new JSONObject(jsonRequest).getJSONArray("result");
            List<Table> list = new ArrayList<Table>();
            for (int i = 0; i < jsonResult.length(); i++)
            {
                JSONObject tableJSON = jsonResult.getJSONObject(i);
                String name = tableJSON.getString("name");
                String label = tableJSON.getString("label");
                String sysid = tableJSON.getString("sys_id");
                String link  = "";
                String value = "";
                if(tableJSON.has("sys_package") && !tableJSON.isNull("sys_package") && !tableJSON.getString("sys_package").isEmpty())
                {
                    JSONObject jsonSysPackage = tableJSON.getJSONObject("sys_package");
                    link = jsonSysPackage.getString("link");
                    value = jsonSysPackage.getString("value");
                }
                list.add(new SNTables(name,label,sysid,link,value));
            }
            snTables = list;
        }

        catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is not in expected format in loadAllSNTableNames(){}",jsonRequest);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is not in expected format in loadAllSNTableNames() function"+jsonRequest,jsonRequest);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        
    }
    public boolean getIsEditable()
    {
        if(getSchemeId() == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public String getSchemeId()
    {
        return request.getParameter("sc_id");
    }

    public List<Field> getAllIssueFields()
    {
        if(fields.size() > 0)
        {
            return fields;
        }
        return new ArrayList<Field>();
    }
    public List<Table> getAllSNTableNames()
    {
         if(snTables.size() > 0)
        {
            Collections.sort(snTables,SNTables.TableLabelComparator);
            return snTables;        
        }
        return new ArrayList<Table>();
    }

    public List<NameValueMapper> getAllSchemes()
    {
        try
        {
            Iterable<Scheme> schemeVariables = aoMappingService.allSchemes();
            List<NameValueMapper> nameValueList = new ArrayList<NameValueMapper>();
            for(Scheme schemeVariable : schemeVariables) {
                nameValueList.add(new NameValueFactory(schemeVariable.getSchemeName(),String.valueOf(schemeVariable.getID())));
            }
            return nameValueList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    public List<Scheme> getSchemes()
    {
        try
        {
            Iterable<Scheme> schemeVariables = aoMappingService.allSchemes();
            List<Scheme> schemeList = new ArrayList<Scheme>();
            for(Scheme schemeVariable : schemeVariables) {
                schemeList.add(schemeVariable);
            }
            return schemeList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    //Fetch the remaining projects which is not mapped to any scheme
    public List<NameValueMapper> getUnSelectedProjects()
    {
        List<NameValueMapper> nameValuePairList = new ArrayList<NameValueMapper>();
        for(NameValueMapper keyVal : getAllProjects())
        {
            boolean hasValue = true;
            for(NameValueMapper valMap : getAllProjectFromScheme())
            {
                if(keyVal.getValue().equalsIgnoreCase(valMap.getValue()))
                {
                    hasValue = false;
                }
            }
            if(hasValue)
            {
                nameValuePairList.add(keyVal);
            }
        }
        return nameValuePairList;
    }
    //To fetch all the ProjectNames which is already assigned to schemes
    public List<NameValueMapper> getAllProjectFromScheme()
    {

        List<NameValueMapper> nameValuePairList = new ArrayList<NameValueMapper>();
        for(Scheme scheme : getSchemes())
        {
            for(ProjectMapping promap : scheme.getProjectMappings())
            {
                nameValuePairList.add(new NameValueFactory(promap.getProjectName(),promap.getProjectName()));
            }
        }
        return nameValuePairList;
    }
    //Fetch the Project names which is assigned by one scheme. Scheme ID will be fetched from Query string parameter
    public List<NameValueMapper> getSelectedProjects()
    {
        Scheme scheme = getScheme();
        List<NameValueMapper> nameValuePairList = new ArrayList<NameValueMapper>();
        for(NameValueMapper keyVal : getAllProjects())
        {
            
            boolean hasValue = false;
            for(ProjectMapping proMap : scheme.getProjectMappings())
            {
                if(keyVal.getValue() == proMap.getProjectName())
                {
                    hasValue = true;
                }
            }
            if(hasValue)
            {
                nameValuePairList.add(keyVal);
            }
        }
        return nameValuePairList;
    }


    public List<ProjectMapping> getProjects()
    {
        try
        {
            Iterable<ProjectMapping> projectVariables = aoMappingService.allProjectMappings();
            List<ProjectMapping> projectList =Lists.newArrayList(projectVariables);//  new ArrayList<Scheme>();
            // for(Scheme schemeVariable : schemeVariables) {
            //     schemeList.add(schemeVariable);
            // }
            return projectList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<NameValueMapper> getAllProjects()
    {
        try
        {
            List<Project> projectVariables = projectManager.getProjectObjects();
            List<NameValueMapper> nameValueList = new ArrayList<NameValueMapper>();
            for(Project project : projectVariables) {
                nameValueList.add(new NameValueFactory(project.getName(),project.getKey()));
            }
            return nameValueList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    //This Method is not used as of now
    public List<Column> getAllSNTableColumnNames(String tableName) throws HSJSONNotFormattedException
    {
        String  jsonRequest = "";
        try
        {
            jsonRequest = serviewNowClient.fetchAllTableColumnNames(tableName);
            List<Column> snTableColumns = new ArrayList<Column>();
            JSONArray columnArray = new JSONObject(jsonRequest).getJSONArray("result");
            for (int i = 0; i < columnArray.length(); i++)
            {
                JSONObject columnsJSON = columnArray.getJSONObject(i);
                String name = columnsJSON.getString("columnname");
                String label = columnsJSON.getString("columnlabel");
                String sysid = columnsJSON.getString("sys_id");
                String type = columnsJSON.getString("type");
                snTableColumns.add(new SNColumns(name,label,sysid,type,""));
            }    
            log.info("All Column Names{}",snTableColumns.size());
            if(snTableColumns.size() > 0)
            {
                Collections.sort(snTableColumns,SNColumns.ColumnLabelComparator);
                return snTableColumns;        
            }
        }
        catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is in expected format in loadAllSNTableNames(){}",jsonRequest);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is in expected format in loadAllSNTableNames() function",jsonRequest);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return new ArrayList<Column>();
    }

    public List<SyncTransactionLog> getAllLogs()
    {
        try
        {
            Iterable<SyncTransactionLog> logVariables = aoMappingService.allSyncTransactionLog();
            List<SyncTransactionLog> logList = new ArrayList<SyncTransactionLog>();
            for(SyncTransactionLog logVariable : logVariables) {
                logList.add(logVariable);
            }
            return logList;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
